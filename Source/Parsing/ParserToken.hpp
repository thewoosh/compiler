/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Parsing/KeywordTokenType.hpp"
#include "Source/Parsing/OperatorType.hpp"
#include "Source/Parsing/ParserTokenType.hpp"
#include "Source/Parsing/PunctuatorType.hpp"
#include "Source/Text/Unicode.hpp"
#include "Source/Text/UString.hpp"
#include "PreprocessorToken.hpp"

namespace Parsing {

    struct ParserToken {
        const PreprocessorToken *associatedPreprocessorToken{nullptr};
        ParserTokenType type;

        explicit constexpr inline
        ParserToken(ParserTokenType type) noexcept
            : type(type)
        {}

        virtual ~ParserToken() = default;

        template<typename Type>
        [[nodiscard]]
        static inline
        const auto &
        GetValue(const std::unique_ptr<ParserToken> &ptr) {
            return static_cast<const Type *>(ptr.get())->value;
        }

        template <typename Type>
        [[nodiscard]] inline const auto *
        As() const noexcept {
            return static_cast<const Type *>(this);
        }
    };

    struct IdentifierToken
            : public ParserToken {
        Unicode::UString value{};

        explicit inline
        IdentifierToken(Unicode::UString &value) noexcept
                : ParserToken(ParserTokenType::IDENTIFIER)
                , value(std::move(value))
        {}

        explicit inline
        IdentifierToken(Unicode::UString &&value) noexcept
                : ParserToken(ParserTokenType::IDENTIFIER)
                , value(std::move(value))
        {}
    };

    struct StringLiteralToken
            : public ParserToken {
        Unicode::UString value{};

        explicit inline
        StringLiteralToken(Unicode::UString &value) noexcept
            : ParserToken(ParserTokenType::STRING_LITERAL)
            , value(std::move(value))
        {}

        explicit inline
        StringLiteralToken(Unicode::UString &&value) noexcept
                : ParserToken(ParserTokenType::STRING_LITERAL)
                , value(std::move(value))
        {}
    };

    struct KeywordToken
            : public ParserToken {
        const KeywordTokenType value;

        explicit constexpr inline
        KeywordToken(KeywordTokenType value) noexcept
            : ParserToken(ParserTokenType::KEYWORD)
            , value(value)
        {}
    };

    struct ConstantToken
            : public ParserToken {
        explicit constexpr inline
        ConstantToken(ParserTokenType type) noexcept
            : ParserToken(type)
        {}
    };

    struct EnumerationConstantToken
            : public ConstantToken {
        Unicode::UString value{};

        explicit inline
        EnumerationConstantToken(Unicode::UString &value) noexcept
            : ConstantToken(ParserTokenType::CONSTANT_ENUMERATION)
            , value(std::move(value))
        {}

        explicit inline
        EnumerationConstantToken(Unicode::UString &&value) noexcept
            : ConstantToken(ParserTokenType::CONSTANT_ENUMERATION)
            , value(std::move(value))
        {}
    };

    struct CharacterConstantToken
            : public ConstantToken {
        Unicode::CodePoint value;

        explicit constexpr inline
        CharacterConstantToken(Unicode::CodePoint value) noexcept
            : ConstantToken(ParserTokenType::CONSTANT_CHARACTER)
            , value(value)
        {}
    };

    struct FloatingConstantToken
            : public ConstantToken {
        float value;

        explicit constexpr inline
        FloatingConstantToken(float value) noexcept
            : ConstantToken(ParserTokenType::CONSTANT_FLOATING)
            , value(value)
        {}
    };

    struct IntegerConstantToken
            : public ConstantToken {
        bool isLong{false};
        bool isLongLong{false};

        explicit constexpr inline
        IntegerConstantToken(const ParserTokenType type) noexcept
            : ConstantToken(type)
        {}
    };

    struct UnsignedIntegerConstantToken
            : public IntegerConstantToken {

        std::uint64_t value;

        explicit constexpr inline
        UnsignedIntegerConstantToken(std::uint64_t value) noexcept
            : IntegerConstantToken(ParserTokenType::CONSTANT_UNSIGNED_INTEGER)
            , value(value)
        {}
    };

    struct SignedIntegerConstantToken
            : public IntegerConstantToken {

        std::int64_t value;

        explicit constexpr inline
        SignedIntegerConstantToken(std::int64_t value) noexcept
            : IntegerConstantToken(ParserTokenType::CONSTANT_SIGNED_INTEGER)
            , value(value)
        {}
    };

    struct OperatorToken
            : public ParserToken {
        OperatorType value;

        explicit constexpr inline
        OperatorToken(OperatorType value) noexcept
            : ParserToken(ParserTokenType::OPERATOR)
            , value(value)
        {}
    };

    struct PunctuatorToken
            : public ParserToken {
        PunctuatorType value;

        explicit constexpr inline
        PunctuatorToken(PunctuatorType value) noexcept
                : ParserToken(ParserTokenType::PUNCTUATOR)
                , value(value)
        {}
    };

    inline std::ostream &
    operator<<(std::ostream &stream, const std::unique_ptr<ParserToken> &token) {
        stream << "ParserToken[type=" << token->type << ", value=";
        switch (token->type) {
            case ParserTokenType::CONSTANT_CHARACTER:
                stream << ParserToken::GetValue<CharacterConstantToken>(token);
                break;
            case ParserTokenType::CONSTANT_ENUMERATION:
                stream << ParserToken::GetValue<EnumerationConstantToken>(token);
                break;
            case ParserTokenType::CONSTANT_FLOATING:
                stream << ParserToken::GetValue<FloatingConstantToken>(token);
                break;
            case ParserTokenType::CONSTANT_SIGNED_INTEGER:
                stream << ParserToken::GetValue<SignedIntegerConstantToken>(token);
                break;
            case ParserTokenType::CONSTANT_UNSIGNED_INTEGER:
                stream << ParserToken::GetValue<UnsignedIntegerConstantToken>(token);
                break;
            case ParserTokenType::IDENTIFIER:
                stream << ParserToken::GetValue<IdentifierToken>(token);
                break;
            case ParserTokenType::KEYWORD:
                stream << ParserToken::GetValue<KeywordToken>(token);
                break;
            case ParserTokenType::OPERATOR:
                stream << ParserToken::GetValue<OperatorToken>(token);
                break;
            case ParserTokenType::PUNCTUATOR:
                stream << ParserToken::GetValue<PunctuatorToken>(token);
                break;
            case ParserTokenType::STRING_LITERAL:
                stream << ParserToken::GetValue<StringLiteralToken>(token);
                break;
            default:
                stream << "unknown";
        }

        stream << ']';

        return stream;
    }

} // namespace Parsing
