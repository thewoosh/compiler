/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <string>
#include <string_view>
#include <utility>

#include "SourceInformation.hpp"

namespace Parsing {

    struct ParserException
            : public std::exception
            , public FormattableMessage {

        std::string_view tag;
        SourceInformation sourceInformation;
        std::string data;

        ParserException(std::string_view tag, SourceInformation sourceInformation)
                : tag(tag)
                , sourceInformation(sourceInformation)
                , data{}
        {}

        ParserException(std::string_view tag, SourceInformation sourceInformation, std::string &&data)
                : tag(tag)
                , sourceInformation(sourceInformation)
                , data(std::move(data))
        {}

        ParserException(const ParserException &parserException) noexcept
            : tag(parserException.tag)
            , sourceInformation(parserException.sourceInformation)
            , data(parserException.data) {
        }

        [[nodiscard]] const char *
        what() const noexcept override {
            return data.c_str();
        }

        [[nodiscard]] std::string_view
        Message() override {
            return data;
        }

        [[nodiscard]] SourceInformation
        Source() override {
            return sourceInformation;
        }

        [[nodiscard]] std::string_view
        Tag() override {
            return tag;
        }

        [[nodiscard]] std::string_view
        TypeName() override {
            return "parser error";
        }

    };

} // namespace Parsing
