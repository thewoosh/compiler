/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iostream>
#include <ostream>

#include "Source/Parsing/FormattableMessage.hpp"
#include "Source/Parsing/SourceInformation.hpp"

namespace Parsing {

    class WarningBuilder
            : public FormattableMessage {
        std::string data;
        std::string_view tag;
        SourceInformation sourceInformation;

        std::stringstream stream{};

        using ReporterType = std::function<void(WarningBuilder &)>;
        ReporterType reporter{};

    public:
        WarningBuilder(std::string_view tag,
                       SourceInformation sourceInformation,
                       ReporterType &&reporter)
                : tag(tag)
                , sourceInformation(sourceInformation)
                , reporter(std::move(reporter)) {
        }

        WarningBuilder(const WarningBuilder &) = delete;

        ~WarningBuilder() {
            if (reporter)
                reporter(*this);
        }

        WarningBuilder &
        operator<<(const auto &v) {
            stream << v;
            return *this;
        }

        [[nodiscard]] std::string_view
        Message() override {
            data = stream.str();
            return data;
        }

        [[nodiscard]] SourceInformation
        Source() override {
            return sourceInformation;
        }

        [[nodiscard]] std::string_view
        Tag() override {
            return tag;
        }

        [[nodiscard]] std::string_view
        TypeName() override {
            return "warning";
        }

    };

} // namespace Parsing
