/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <iostream>

#include "AST/TreeConstructor.hpp"
#include "AST/ConstructionException.hpp"
#include "Base/ExitCodes.hpp"
#include "CodeGeneration/Generator.hpp"
#include "Parsing/Parser.hpp"
#include "Runtime/Engine.hpp"
#include "SemanticAnalysis/Analyzer.hpp"
#include "Source/Settings/SettingsTable.hpp"

int main() {
    Settings::SettingsTable settingsTable{};

    Unicode::UString string(
            R"(int main(void) {
                return 128 + 127;
            })"
    );

    Parsing::Parser parser(string);

    try {
        parser.Run();
    } catch (Parsing::ParserException &exception) {
        exception.PrintMessage(&parser, std::cerr);
        return Base::ExitCodes::ParserFailure;
    }

    AST::TreeConstructor treeConstructor{parser.OutputTokens()};

    try {
        treeConstructor.Run();
    } catch (AST::ConstructionException &exception) {
        exception.PrintMessage(&parser, std::cerr);
        return Base::ExitCodes::ASTFailure;
    }

    SemanticAnalysis::Analyzer semanticAnalyzer{treeConstructor.GetRootNode(), settingsTable};
    if (!semanticAnalyzer.Invoke()) {
        std::cout << "Semantic analysis phase failed!\n";
        return Base::ExitCodes::SemanticAnalysisFailure;
    }

    CodeGeneration::Generator generator{treeConstructor.GetRootNode()};
    generator.Run();

    std::cout << "main> Running code...\n";
    Runtime::Engine engine{&generator};
    const int retval = engine.Run();
    std::cout << "main> Program finished with exit code " << retval << '\n';


    return Base::ExitCodes::Success;
}
