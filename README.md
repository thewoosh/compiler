# Compiler
A C89 compiler for fun. Contains some BSD-2-clause licensed components taken
from the [WebEngine](https://gitlab.com/thewoosh/WebEngine) project. The main
code is licensed under the
[Mozilla Public License v2.0](http://mozilla.org/MPL/2.0/).