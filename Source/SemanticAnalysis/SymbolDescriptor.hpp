/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/SemanticAnalysis/SymbolType.hpp"

namespace SemanticAnalysis {

    struct SymbolDescriptor {
        SymbolType type;

        [[nodiscard]] inline constexpr explicit
        SymbolDescriptor(SymbolType type)
                : type(type) {
        }

        SymbolDescriptor(SymbolDescriptor &&) = default;
        SymbolDescriptor(const SymbolDescriptor &) = default;
        SymbolDescriptor &operator=(SymbolDescriptor &&) = default;
        SymbolDescriptor &operator=(const SymbolDescriptor &) = default;
    };

} // namespace SemanticAnalysis
