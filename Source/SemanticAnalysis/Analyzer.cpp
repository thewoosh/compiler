/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Analyzer.hpp"

#include <iostream>

#include <cassert>
#include <cstdio>

#include "Source/Base/Portability.hpp"

namespace SemanticAnalysis {

    bool Analyzer::Invoke() {
        return InspectNode(rootNode);
    }

    bool Analyzer::InspectNode(const AST::Node *node) {
        printf("[SemanticAnalysis] Encounter Node{type=%s, children=%zu}\n", AST::TranslateNodeTypeToString(node->type), std::size(node->children));

        switch (node->type) {
            case AST::NodeType::ROOT:
                for (const auto &childNode : node->children) {
                    if (!InspectNode(childNode.get()))
                        return false;
                }
                return true;
            case AST::NodeType::BLOCK:
            case AST::NodeType::FUNCTION:
                return InspectFunction(INHERITANCE_CAST<const AST::FunctionNode *>(node));
            case AST::NodeType::STATEMENT_EXPRESSION:
                break;
            case AST::NodeType::STATEMENT_RETURN:
                return InspectReturnStatement(INHERITANCE_CAST<const AST::ReturnStatementNode *>(node));
            default:
                assert(false);
        }

        return false;
    }

    bool Analyzer::InspectFunction(const AST::FunctionNode *node) {
        assert(functionPtr == nullptr);

        symbolStack.PushFrame();
        functionPtr = node;

        // TODO: for_each the declarators ArgumentList and add them to the
        //       symbol stack; function->declarator->argumentList

        for (const auto &childNode : node->children) {
            if (!InspectNode(childNode.get()))
                return false;
        }

        symbolStack.PopFrame();
        functionPtr = nullptr;

        return true;
    }

    bool Analyzer::InspectReturnStatement(const AST::ReturnStatementNode *node) {
        // return statement _node_ shouldn't be created outside a function.
        assert((functionPtr != nullptr));

        if (node->expression && functionPtr->declarator->returnType.isKeyword &&
            functionPtr->declarator->returnType.keyword == Parsing::KeywordTokenType::VOID) {
            puts("[SemanticAnalysis] Return statement without expression when return type is not void");
            return false;
        }

        if (!node->expression &&
                (!functionPtr->declarator->returnType.isKeyword ||
                functionPtr->declarator->returnType.keyword == Parsing::KeywordTokenType::VOID)) {
            puts("[SemanticAnalysis] Return statement without expression when return type is not void");
            return false;
        }

        return true;
    }

} // namespace SemanticAnalysis
