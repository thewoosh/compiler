/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace SemanticAnalysis {

    /**
     * NOTES:
     * (1) Signedness of char types is implementation-defined as per the
     *     standard (C89 A.6.3.3 - 8). By default, the type is signed to follow
     *     GCC, Clang and MSVC.
     */
    enum class SymbolType {

        FUNCTION,
        STRUCT,
        PRIMARY_DOUBLE,
        PRIMARY_FLOAT,

        // 'int'
        // 'signed int'
        PRIMARY_INT,

        // 'signed char'
        // ('char')
        PRIMARY_SIGNED_CHAR,

        // 'short'
        // 'short int'
        // 'signed short'
        // 'signed short int'
        PRIMARY_SHORT,

        // 'long'
        // 'long int'
        // 'signed long'
        // 'signed long int'
        PRIMARY_LONG,

        // 'unsigned'
        // 'unsigned int'
        PRIMARY_UNSIGNED,

        // 'unsigned char'
        // ('char')
        PRIMARY_UNSIGNED_CHAR,

        // 'unsigned short'
        // 'unsigned short int'
        PRIMARY_UNSIGNED_SHORT,

        // 'unsigned long'
        // 'unsigned long int'
        PRIMARY_UNSIGNED_LONG,

    };

} // namespace SemanticAnalysis
