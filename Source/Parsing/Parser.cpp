/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Parser.hpp"

#include <vector>

#include "Source/Algorithm.hpp"
#include "Source/Parsing/PreprocessorUtility.hpp"
#include "Source/Parsing/ParserError.hpp"
#include "Source/Parsing/WarningBuilder.hpp"

namespace Parsing {

    //
    // Parses the input string.
    //
    // TODO Currently the parser expects a document with LINE FEEDs as
    //      new-lines. Maybe pre-pre-process the document to replace CR and
    //      CRLF with LF?
    //
    Parser::Parser(const Unicode::UString &string)
        : string(CreateInternalDataForUString(string.length() + 1))
                                                   // + 1 for LF
    {
        auto it = std::begin(string);
        bool first = true;
        auto questionMarks = 0;
        for (; it != std::end(string); ++it) {
            const bool wasFirst = first;
            first = true;

            if (questionMarks == 2) {
                questionMarks = 0;
                Unicode::CodePoint codePoint = TranslateTrigraph(*it);
                if (codePoint != Unicode::NULL_CHARACTER) {
                    this->string += codePoint;
                    continue;
                }

                this->string += Unicode::QUESTION_MARK;
                this->string += Unicode::QUESTION_MARK;

                // ???= translates to ?#, so don't continue here
            }

            // a backslash consumed the following newline
            if (!wasFirst && *it == Unicode::LINE_FEED && *(it - 1) == Unicode::REVERSE_SOLIDUS) {
                continue;
            }

            if (*it == Unicode::QUESTION_MARK) {
                questionMarks++;
                continue;
            }

            this->string += *it;
        }

        // Make sure string ends with a newline
        if (this->string.back() != Unicode::LINE_FEED) {
            if (this->string.back() == Unicode::REVERSE_SOLIDUS) {
                this->string[this->string.length()] = Unicode::LINE_FEED;
            } else {
                this->string += Unicode::LINE_FEED;
            }
        }
    }

    void
    Parser::Run() {
        RunPreprocessor();

        PrintDebugInfo();

        TranslatePreprocessorTokensToParserTokens();

        std::cout << "Parsed tokens (" << std::size(parserTokens) << "):\n";
        std::size_t position = 0;
        for (const auto &token : parserTokens) {
            std::cout << "  (" << (++position) << "): " << token << '\n';
        }
    }

    void Parser::RunPreprocessor() {
        PrepareSourceInformation();
        PreparePredefinedMacros();

        Preprocess();
        ExecutePreprocessorDirectives();
        ReplaceEscapeSequences();
        ConcatenateAdjacentStringLiterals();
    }

    void Parser::PreparePredefinedMacros() {
        std::vector<PreprocessorToken> oneVector{
                PreprocessorToken{
                    {runtimeConstants.fileNameBuiltin},
                    PreprocessorTokenType::PP_NUMBER,
                    Unicode::UString{"1"}
                }
        };

        std::vector<PreprocessorToken> fileNameVector{
                PreprocessorToken{
                        {runtimeConstants.fileNameBuiltin},
                        PreprocessorTokenType::STRING_LITERAL,
                        fileName
                }
        };

        preprocessor.macroTable.emplace(Unicode::UString{"__STDC__"}, MacroEntry{nullptr, oneVector});
        preprocessor.macroTable.emplace(Unicode::UString{"__FILE__"}, MacroEntry{nullptr, std::move(fileNameVector)});

        for (const auto &macro : runtimeConstants.builtinMacros) {
            preprocessor.macroTable.emplace(macro, MacroEntry{nullptr, oneVector});
        }
    }

    [[nodiscard]] SourceInformation
    Parser::CreateSourceInformation() {
        SourceInformation info{fileName, 1, 0};

        const auto end = std::end(string);

        for (auto it = std::begin(string); it != end; ++it) {
            if (std::distance(it, end) == std::distance(characterIt, end)) {
                return info;
            }

            ++info.column;

            if (*it == Unicode::LINE_FEED) {
                info.column = 0;
                ++info.line;
            }
        }

        // is reached when tokens are handled, and the raw input characters
        // have been consumed
        info.column = 0;
        info.line = 0;
        return info;
    }

    void
    Parser::PrepareSourceInformation() {
        //
        // Pre-allocate strings
        //
        std::size_t length{0};
        for (Unicode::CodePoint codePoint : string) {
            if (codePoint == Unicode::LINE_FEED) {
                std::vector<Unicode::CodePoint> vec{};
                vec.reserve(length);
                lines.emplace_back(std::move(vec));
                length = 0;
                continue;
            }

            ++length;
        }

        //
        // Store strings
        //
        auto it = std::begin(lines);
        for (Unicode::CodePoint codePoint : string) {
            if (codePoint == Unicode::LINE_FEED) {
                ++it;
                continue;
            }

            *it += codePoint;
        }
    }

    void Parser::PrintDebugInfo() {
//        std::cout << "Preprocessed document: \"" << string << "\"\n";

        std::cout << "Preprocessor tokens (" << preprocessor.tokens.size() << "):\n";
        for (std::size_t i = 0; i < preprocessor.tokens.size(); i++) {
            const auto &token = preprocessor.tokens[i];
            std::cout << "  (" << (i + 1) << "): " << token << '\n';
        }
    }

    void Parser::Preprocess() {
        characterIt = std::begin(string);
        for (; characterIt != std::end(string); ++characterIt) {
            Unicode::CodePoint character = *characterIt;
            if (!preprocessor.inCommentAsterisk &&
                character == Unicode::SOLIDUS &&
                (characterIt + 1) != std::end(string) &&
                *(characterIt + 1) == Unicode::ASTERISK)
            {
                ++characterIt;
                preprocessor.inComment = true;
                preprocessor.inCommentAsterisk = false;
                continue;
            }

            if (preprocessor.inComment) {
                PreprocessComment(character);
                continue;
            }

            if (preprocessor.tokens.empty()) {
                PreprocessFirst(character);
                continue;
            }

            auto &back = preprocessor.tokens.back();
            if (!preprocessor.inIncludeStatement &&
                    back.type == PreprocessorTokenType::IDENTIFIER) {
                DetectIncludeStatement(back, character);
            }

            if (preprocessor.inIncludeStatement) {
                PreprocessIncludeStatement(character);
                continue;
            }

            if (preprocessor.inNumber) {
                PreprocessDigit(character);
                if (preprocessor.inNumber) {
                    continue;
                }
            }

            PreprocessorToken &previousToken = preprocessor.tokens.back();

            if (!previousToken.final) {
                auto size = std::size(previousToken.data);
                bool lastWasReverseSolidus = size >= 2 &&
                        previousToken.data.back() == Unicode::REVERSE_SOLIDUS &&
                        previousToken.data[std::size(previousToken.data) - 2] != Unicode::REVERSE_SOLIDUS;

                if (previousToken.type == PreprocessorTokenType::STRING_LITERAL) {
                    if (character == Unicode::QUOTATION_MARK &&
                        !lastWasReverseSolidus) {
                        previousToken.final = true;
                    } else {
                        previousToken.data += character;
                    }
                    continue;
                }
                if (previousToken.type == PreprocessorTokenType::CHARACTER_CONSTANT) {
                    if (character == Unicode::APOSTROPHE &&
                        !lastWasReverseSolidus) {
                        previousToken.final = true;
                    } else {
                        previousToken.data += character;
                    }
                    continue;
                }
            }

            if (previousToken.type != PreprocessorTokenType::IDENTIFIER &&
                Unicode::IsDigit(character)) {
                PreprocessFirstNumber(character);
                continue;
            }

            if (IsIdentifierCharacter(character)) {
                switch (previousToken.type) {
                    case PreprocessorTokenType::HEADER_NAME:
                    case PreprocessorTokenType::IDENTIFIER:
                        previousToken.data += character;
                        break;
                    case PreprocessorTokenType::OPERATOR:
                    case PreprocessorTokenType::PUNCTUATOR:
                    case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR:
                    case PreprocessorTokenType::WHITESPACE:
                        preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::IDENTIFIER, character);
                        break;
                    default:
                        FireParserError("identifier-character") << "Previous=" << previousToken;
                        break;
                }
            } else if (IsOperatorOrPunctuator(character)) {
                PreprocessOperatorOrPunctuator(character);
            } else if (character == Unicode::QUOTATION_MARK) {
                preprocessor.EmplaceToken(CreateSourceInformation(),PreprocessorTokenType::STRING_LITERAL);
            } else if (character == Unicode::APOSTROPHE) {
                preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::CHARACTER_CONSTANT);
            } else if (IsWhitespaceCharacter(character)) {
                if (character == Unicode::LINE_FEED)
                    preprocessor.inIncludeStatement = false;
                if (previousToken.type == PreprocessorTokenType::WHITESPACE)
                    previousToken.data += character;
                else
                    preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::WHITESPACE, character);
            } else {
                preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::OTHER, character);
            }
        }
    }

    void
    Parser::PreprocessDigit(Unicode::CodePoint character) {
        auto &token = preprocessor.tokens.back();

        if (IsWhitespaceCharacter(character)) {
            token.final = true;
            preprocessor.inNumber = false;
            return;
        }

        if (!Unicode::IsASCIIAlphaNumeric(character) &&
                character != Unicode::LOW_LINE &&
                !((character == Unicode::HYPHEN_MINUS ||
                   character == Unicode::PLUS_SIGN) &&
                  (token.data.back() == Unicode::LATIN_SMALL_LETTER_E &&
                   token.data.back() == Unicode::LATIN_CAPITAL_LETTER_E))) {
            token.final = true;
            preprocessor.inNumber = false;
            return;
        }

        token.data += character;
    }

    void Parser::PreprocessFirstNumber(Unicode::CodePoint character) {
        PreprocessorToken previousTokenCopy = preprocessor.tokens.back();
        bool consumeLastTokenData = previousTokenCopy.data == Unicode::FULL_STOP;
        if (consumeLastTokenData)
            preprocessor.tokens.pop_back();

        // dont use EmplaceToken because the function may process the token
        // weird since the previous token may have been popped.
        auto &token = preprocessor.tokens.emplace_back(CreateSourceInformation(), PreprocessorTokenType::PP_NUMBER);

        if (consumeLastTokenData)
            token.data = std::move(previousTokenCopy.data);

        token.data += character;
        preprocessor.inNumber = true;
    }

    void Parser::DetectIncludeStatement(const PreprocessorToken &token,
                                        Unicode::CodePoint character) {
        if (!IsIdentifierCharacter(character) &&
            std::size(preprocessor.tokens) >= 3 &&
            token.type == PreprocessorTokenType::IDENTIFIER &&
            token.data == runtimeConstants.directiveInclude)
        {
            const PreprocessorToken &backToken = preprocessor.tokens[std::size(preprocessor.tokens) - 2];
            if (backToken.type == PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR &&
                backToken.data == Unicode::NUMBER_SIGN)
            {
                preprocessor.inIncludeStatement = true;
                preprocessor.includeCharacter = Unicode::NULL_CHARACTER;
            }
        }
    }

    void Parser::PreprocessIncludeStatement(Unicode::CodePoint character) {
        if (preprocessor.includeCharacter != Unicode::NULL_CHARACTER) {
            if (DoesCharacterEndIncludeStatement(character, preprocessor.includeCharacter)) {
                std::cout << "Include header: " << preprocessor.tokens.back() << "\n";
                preprocessor.includeCharacter = Unicode::NULL_CHARACTER;
                preprocessor.inIncludeStatement = false;
                return;
            }

            preprocessor.tokens.back().data += character;
        } else if (character == Unicode::LESS_THAN_SIGN || character == Unicode::QUOTATION_MARK) {
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::HEADER_NAME);
            preprocessor.includeCharacter = character;
        } else {
            if (!IsWhitespaceCharacter(character)) {
                FireParserError("macro.format.include") << "Invalid character after #include: '" << static_cast<char>(character) << '\n';
            }
        }
    }

    void Parser::AppendSingleOperatorOrPunctuator(Unicode::CodePoint character) {
        // punctuator is [ ] ( ) { } * , : = ; ... #
        PreprocessorToken &token = preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR, character);
        if (character == Unicode::LEFT_SQUARE_BRACKET ||
            character == Unicode::RIGHT_SQUARE_BRACKET ||
            character == Unicode::LEFT_PARENTHESIS ||
            character == Unicode::RIGHT_PARENTHESIS ||
            character == Unicode::LEFT_CURLY_BRACKET ||
            character == Unicode::RIGHT_CURLY_BRACKET ||
            character == Unicode::ASTERISK ||
            character == Unicode::COMMA ||
            character == Unicode::COLON ||
            character == Unicode::EQUALS_SIGN ||
            character == Unicode::FULL_STOP ||
            character == Unicode::NUMBER_SIGN) {
            return;
        }

        if (character == Unicode::SEMICOLON) {
            token.type = PreprocessorTokenType::PUNCTUATOR;
        } else {
            token.type = PreprocessorTokenType::OPERATOR;
        }
    }

    void Parser::PreprocessOperatorOrPunctuator(Unicode::CodePoint character) {
        /**
         * [ ] ( ) . ->
         * ++ -- & * + - ~ ! sizeof
         * / % << >> < > <= >= == != ^ | && || ? :
         * = *= /= %= += -= <<= >>= &= ^= |= , # ##
         *
         */
        PreprocessorToken &previousToken = preprocessor.tokens.back();
        if (previousToken.type == PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR ||
            previousToken.type == PreprocessorTokenType::OPERATOR) {
            if (previousToken.data.length() == 1) {
                if (character == Unicode::EQUALS_SIGN) {
                    switch (previousToken.data.front()) {
                        case Unicode::ASTERISK:             // *= multiplication assigment operator
                        case Unicode::SOLIDUS:              // /= division assigment operator
                        case Unicode::PERCENTAGE_SIGN:      // %= remainder assigment operator
                        case Unicode::PLUS_SIGN:            // += addition assignment operator
                        case Unicode::HYPHEN_MINUS:         // -= subtraction assignment operator
                        case Unicode::AMPERSAND:            // &= bitwise and assigment operator
                        case Unicode::CIRCUMFLEX_ACCENT:    // ^= bitwise xor assigment operator
                        case Unicode::VERTICAL_LINE:        // |= bitwise or assigment operator
                        case Unicode::EQUALS_SIGN:          // == equality operator
                        case Unicode::LESS_THAN_SIGN:       // <= less than or equal to operator
                        case Unicode::GREATER_THAN_SIGN:    // >= greater than or equal to operator
                        case Unicode::EXCLAMATION_MARK:     // != inequality operator
                            previousToken.data += character;
                            previousToken.final = true;
                            previousToken.type = PreprocessorTokenType::OPERATOR;
                            return;
                        default:
                            previousToken.final = true;
                            AppendSingleOperatorOrPunctuator(character);
                            return;
                    }
                }

                if (character == previousToken.data.front() &&
                        (IsRepeatingCharacterAnOperator(character) ||
                         character == Unicode::FULL_STOP)) {
                    previousToken.data += character;
                    // not explicitly final, <<= and >>=
                    previousToken.type = PreprocessorTokenType::OPERATOR;
                    return;
                }
            } else if (previousToken.data.length() == 2 && previousToken.data[0] == previousToken.data[1]) {
                if (character == Unicode::EQUALS_SIGN &&
                      (previousToken.data[0] == Unicode::LESS_THAN_SIGN
                    || previousToken.data[0] == Unicode::GREATER_THAN_SIGN)) {
                    previousToken.data += character;
                    previousToken.type = PreprocessorTokenType::OPERATOR;
                    previousToken.final = true;
                    return;
                }
                if (previousToken.data[0] == Unicode::FULL_STOP) {
                    if (character != Unicode::FULL_STOP) {
                        FireParserError("punctuator ...") << "Character sequence was incorrect: \".." << static_cast<char>(character) << '"';
                    } else {
                        previousToken.data += Unicode::FULL_STOP;
                        previousToken.final = true;
                        previousToken.type = PreprocessorTokenType::PUNCTUATOR;
                        return;
                    }
                }
            }
        }

        AppendSingleOperatorOrPunctuator(character);
    }

    void Parser::PreprocessComment(Unicode::CodePoint character) {
        if (preprocessor.inCommentAsterisk) {
            if (character == Unicode::SOLIDUS) {
                // end of comment
                preprocessor.inCommentAsterisk = false;
                preprocessor.inComment = false;
                preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::WHITESPACE, ' ');
            } else {
                preprocessor.inCommentAsterisk = false;
            }
        } else if (character == Unicode::ASTERISK) {
            preprocessor.inCommentAsterisk = true;
        } else {
            // ignore the character
        }
    }

    void Parser::PreprocessFirst(Unicode::CodePoint character) {
        if (character == Unicode::LOW_LINE || Unicode::IsASCIIAlphaNumeric(character)) {
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::IDENTIFIER, character);
        } else if (IsOperatorOrPunctuator(character)) {
            AppendSingleOperatorOrPunctuator(character);
        } else if (character == Unicode::QUOTATION_MARK) {
            // do include the quotation mark to indicate the type of the string literal
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::STRING_LITERAL, character);
        } else if (character == Unicode::APOSTROPHE) {
            // do include the quotation mark to indicate the type of the string literal
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::CHARACTER_CONSTANT, character);
        } else if (IsWhitespaceCharacter(character)) {
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::WHITESPACE, character);
        } else {
            preprocessor.EmplaceToken(CreateSourceInformation(), PreprocessorTokenType::OTHER, character);
            std::cout << "[" << __FUNCTION__ << "] Unknown character: " << character << '\n';
        }
    }

    void
    Parser::SkipWhitespaceInMacro(
            std::vector<PreprocessorToken>::iterator &it,
            const std::vector<PreprocessorToken>::iterator &end) {
        if (it->type == PreprocessorTokenType::WHITESPACE &&
            it->data.contains(Unicode::LINE_FEED)) {
            currentToken = &*it;
            FireParserError("macro.format") << "Macro abruptly ended (new line)";
        }

        ++it;

        while (it != end) {
            if (it->type != PreprocessorTokenType::WHITESPACE)
                return;
            if (it->data.contains(Unicode::LINE_FEED)) {
                currentToken = {&*it};
                FireParserError("macro.format") << "Macro abruptly ended (new line)";
            }
            ++it;
        }

        FireParserError("macro.format") << "Macro abruptly ended (EOF)";
    }


    void Parser::ExecutePreprocessorDirectives() {
        std::vector<PreprocessorToken> tokens = std::move(preprocessor.tokens);

        preprocessor.tokens.reserve(tokens.size());

        for (auto it = std::begin(tokens); it < std::end(tokens); ++it) {
            ContinueDirectivesExecutionProcessing(it, tokens);

            if (it != std::end(tokens) &&
                    std::prev(it)->type == PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR &&
                    std::prev(it)->data == Unicode::NUMBER_SIGN &&
                    it->type == PreprocessorTokenType::IDENTIFIER) {
                currentToken = &*it;
                ParserError("directive.location") << "Unexpected directive";
            }
        }
    }

    void
    Parser::ContinueDirectivesExecutionProcessing(
            PreprocessorTokenIterator &it,
            PreprocessorTokenList &tokens
    ) {
        for (; it < std::end(tokens); ++it) {
            ProcessTokensUntilConditionalDirective(it, tokens);
            if (it == std::end(tokens)) {
                return;
            }

            PreprocessorToken &previousToken = *(it - 1);

            if (previousToken.type != PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR ||
                previousToken.data != Unicode::NUMBER_SIGN) {
                currentToken = &previousToken;
                ParserError("preprocessor-directives-execution") << "Illegal state!";
            }

            PreprocessorToken &directiveNameToken = *it;
            currentToken = &directiveNameToken;

            if (directiveNameToken.data == runtimeConstants.directiveEndif) {
                return;
            }
            ++it;

            if (directiveNameToken.data == runtimeConstants.directiveIfdef) {
                ProcessConditionalMacroExistence(it, tokens, true);
            } else if (directiveNameToken.data == runtimeConstants.directiveIfndef) {
                ProcessConditionalMacroExistence(it, tokens, false);
            } else if (directiveNameToken.data == runtimeConstants.directiveElse ||
                       directiveNameToken.data == runtimeConstants.directiveElif) {
                do {
                    ConsumeInvisibleTokensInDirectivesExecution(it, tokens);
                } while (it->data != runtimeConstants.directiveEndif);
                return;
            } else {
                ParserError("directives.name") << "Unknown directive.";
            }
        }
    }

    void Parser::ProcessTokensUntilConditionalDirective(
            PreprocessorTokenIterator &it,
            PreprocessorTokenList &tokens) {
        currentToken = &*it;

        if (std::distance(it, std::end(tokens)) < 0)
            throw std::runtime_error("illegal args");

        for (; it != std::end(tokens); ++it) {
            PreprocessorToken &token = *it;
            currentToken = &token;

            switch (token.type) {
                case PreprocessorTokenType::WHITESPACE:
                    if (token.data.contains(Unicode::LINE_FEED)) {
                        validLocationForDirective = true;
                    }

                    preprocessor.tokens.push_back(token);
                    break;
                case PreprocessorTokenType::IDENTIFIER: {
                    auto macroIt = preprocessor.macroTable.find(token.data);
                    if (macroIt != std::end(preprocessor.macroTable)) {
                        std::copy(std::cbegin(macroIt->second.tokens), std::cend(macroIt->second.tokens),
                                  std::back_inserter(preprocessor.tokens));
                        it += std::size(macroIt->second.tokens);
                    } else if (Algorithm::Contains(pragmaOptions.poison.blockedList, token.data)) {
                        switch (pragmaOptions.poison.level) {
                            case PragmaOptions::Poison::Level::ERROR:
                                FireParserError("poison") << "use of poisoned identifier \"" << token.data << '"';
                                break;
                            case PragmaOptions::Poison::Level::WARNING:
                                FireWarning("poison") << "use of poisoned identifier \"" << token.data << '"';
                                break;
                            default:
                                break;
                        }
                    } else {
                        preprocessor.tokens.emplace_back(std::move(token));
                    }
                } break;
                case PreprocessorTokenType::OPERATOR:
                case PreprocessorTokenType::PUNCTUATOR:
                case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR:
                    if (token.data != Unicode::NUMBER_SIGN || ++it == std::cend(tokens)) {
                        preprocessor.tokens.emplace_back(std::move(token));
                        validLocationForDirective = false;
                    } else {
                        if (!validLocationForDirective) {
                            FireParserError("directive.location") << "Invalid location for directive";
                            return;
                        }

                        auto &nextToken = *it;
                        currentToken = &nextToken;
                        if (nextToken.type != PreprocessorTokenType::IDENTIFIER) {
                            FireParserError("directive.format") << "Directive is followed by a " << nextToken.type;
                        }

                        ++it;

                        if (nextToken.data == runtimeConstants.directivePragma) {
                            ProcessPragmaDirective(it, tokens);
                        } else if (nextToken.data == runtimeConstants.directiveInclude) {
                            // The handling of this is done at the 3 preprocessor stage
                            // TODO consume the tokens associated with this directive
                            break;
                        } else if (nextToken.data == runtimeConstants.directiveDefine) {
                            SkipWhitespaceInMacro(it, std::end(tokens));
                            if (it->type == PreprocessorTokenType::IDENTIFIER) {
                                ProcessDefineMacro(it, tokens);
                                break;
                            }
                            FireParserError("directive.format.define")
                                    << "#define should be followed by an identifier, not an " << nextToken.type;
                        } else if (nextToken.data == runtimeConstants.directiveUndef) {
                            SkipWhitespaceInMacro(it, std::end(tokens));
                            if (it->type != PreprocessorTokenType::IDENTIFIER) {
                                FireParserError("directive.format.undef") << "#define is followed by a "
                                                                          << nextToken.type;
                            } else {
                                ProcessUndefMacro(it);
                            }
                        } else {
                            // handle by the caller
                            --it; // restore name of directive to caller
                            currentToken = &*it;
                            return;
                        }
                    }
                    break;
                default:
                    preprocessor.tokens.emplace_back(std::move(token));
                    validLocationForDirective = false;
                    break;
            }
        }
    }

    void Parser::ConsumeIfStatement(decltype(preprocessor.tokens)::iterator &it,
                                    decltype(preprocessor.tokens) &tokens) {
        bool validLocationForMacro = false;
        for (; it != std::cend(tokens); ++it) {
            switch (it->type) {
                case PreprocessorTokenType::WHITESPACE:
                    if (it->data.contains(Unicode::LINE_FEED)) {
                        validLocationForMacro = true;
                    }
                    break;
                case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR:
                    if (it->data == Unicode::NUMBER_SIGN) {
                        if (!validLocationForMacro)
                            break;

                        const std::size_t tokensLeft = std::distance(it, std::end(tokens));
                        if (tokensLeft < 2)
                            // skip the tokens, since almost at EOF and no useful
                            // tokens can be processed
                            return;

                        ++it;
                        if (it->type == PreprocessorTokenType::IDENTIFIER) {
                            if (it->data == runtimeConstants.directiveEndif ||
                                it->data == runtimeConstants.directiveElse ||
                                it->data == runtimeConstants.directiveElif) {
                                return;
                            }

                            if (it->data == runtimeConstants.directiveIf ||
                                it->data == runtimeConstants.directiveIfdef ||
                                it->data == runtimeConstants.directiveIfndef) {
                                ConsumeIfStatement(it, tokens);
                            }
                        }
                    }
                    break;
                default:
                    validLocationForMacro = false;
                    break;
            }
        }
    }

    void Parser::ProcessConditionalMacroExistence(
            decltype(preprocessor.tokens)::iterator &it,
            decltype(preprocessor.tokens) &tokens,
            bool shouldMacroExists) {
        if (it->type == PreprocessorTokenType::WHITESPACE)
            SkipWhitespaceInMacro(it, std::end(tokens));

        if (it->type != PreprocessorTokenType::IDENTIFIER) {
            currentToken = &*it;
            FireParserError((shouldMacroExists ? "directive.ifdef.format" : "directive.ifndef.format")) << "Expected an macro identifier, but a " << *it << " was given";
        }

        bool status = Algorithm::Contains(preprocessor.macroTable, it->data);

        if (status == shouldMacroExists) {
            ++it;
            currentToken = &*it;
            ContinueDirectivesExecutionProcessing(it, tokens);
            if (it == std::end(tokens))
                goto unexpectedEOF; // TODO replace goto with function
            //      that throws an exception
            if (it->data != runtimeConstants.directiveEndif) {
                currentToken = &*it;
                FireParserError((shouldMacroExists ?
                                 "directive.ifdef.endif" :
                                 "directive.ifndef.endif")) << "Block isn't closed with an #endif";
                return;
            }
        } else {
            ConsumeInvisibleTokensInDirectivesExecution(it, tokens);

            if (it == std::end(tokens)) {
                unexpectedEOF:
                std::cout << "End reached in parent of ConsumeInvisibleTokensInDirectivesExecution\n";
                std::exit(EXIT_FAILURE);
            }

            if (it->type != PreprocessorTokenType::IDENTIFIER) {
                // TODO this is probably subject to a syntax error
                std::cout << "Illegal state in " << __FUNCTION__ << "\n";
                std::exit(EXIT_FAILURE);
            }

            if (it->data == runtimeConstants.directiveEndif) {
                ++it;
                return;
            }

            if (it->data == runtimeConstants.directiveElse) {
                ++it;
                currentToken = &*it;
                ContinueDirectivesExecutionProcessing(it, tokens);
                if (it == std::end(tokens))
                    goto unexpectedEOF; // TODO replace goto with function
                                        //      that throws an exception
                if (it->data != runtimeConstants.directiveEndif) {
                    currentToken = &*it;
                    FireParserError((shouldMacroExists ?
                        "directive.ifdef.endif" :
                        "directive.ifndef.endif")) << "Block isn't closed with an #endif";
                    return;
                }

                return;
            }

            if (it->data == runtimeConstants.directiveIfdef) {
                ++it;

                return;
            }

            currentToken = &*it;
            FireParserError("TODO") << "Unknown directive in #if(n)def-non-condition";
        }
    }

    void Parser::ConsumeInvisibleTokensInDirectivesExecution(
            PreprocessorTokenIterator &it,
            PreprocessorTokenList &tokens) {
        std::size_t ifBlocks = 0;

        for (; it != std::end(tokens); ++it) {
            switch (it->type) {
                case PreprocessorTokenType::WHITESPACE:
                    if (it->data.contains(Unicode::LINE_FEED))
                        validLocationForDirective = true;
                    break;
                case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR:
                    if (it->data == Unicode::NUMBER_SIGN) {
                        ++it;
                        if (it == std::cend(tokens))
                            return; // TODO handle EOF here

                        currentToken = &*it;

                        if (it->data == runtimeConstants.directiveEndif) {
                            if (ifBlocks == 0) {
                                return;
                            }

                            --ifBlocks;
                        } else if (it->data == runtimeConstants.directiveIf ||
                                   it->data == runtimeConstants.directiveIfdef ||
                                   it->data == runtimeConstants.directiveIfndef) {
                            ++ifBlocks;
                        } else if (it->data == runtimeConstants.directiveElse ||
                                   it->data == runtimeConstants.directiveElif) {
                            if (ifBlocks == 0) {
                                return;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    void Parser::ProcessDefineMacro(PreprocessorTokenIterator &it,
                                    PreprocessorTokenList &tokens) {
        currentToken = &*it;

        PreprocessorToken *identifierTokenPtr = &*it;
        Unicode::UString identifierDataCopy = it->data;
        if (warnings.macroIdentifierIsKeyword) {
            auto keywordIt = runtimeConstants.keywordList.find(identifierDataCopy);
            if (keywordIt != std::cend(runtimeConstants.keywordList)) {
                FireWarning("macro.define.keyword") << "#define defines \"" << identifierDataCopy << "\" which is an keyword.";
            }
        }

        CheckDefinableMacroIdentifier(identifierDataCopy, DefineAction::DEFINE);

        ++it;
        std::vector<PreprocessorToken> content{};
        if (it->type != PreprocessorTokenType::WHITESPACE) {
            FireParserError("macro.define.replacement-list")
                << "#define <macro name> should be followed by optionally whitespace and a list with tokens, not: "
                << *it;
        }

        if (!it->data.contains(Unicode::LINE_FEED)) {
            ++it;
            for (; it != std::cend(tokens); ++it) {
                if (it->type == PreprocessorTokenType::WHITESPACE &&
                    it->data.contains(Unicode::LINE_FEED)) {
                    break;
                }
                if (it->type == PreprocessorTokenType::IDENTIFIER) {
                    auto macroIt = preprocessor.macroTable.find(it->data);
                    if (macroIt != std::end(preprocessor.macroTable)) {
                        std::copy(std::cbegin(macroIt->second.tokens), std::cend(macroIt->second.tokens),
                                  std::back_inserter(content));
                        break;
                    }
                }
                content.emplace_back(*it);
            }
        }

        if (it == std::cend(tokens)) {
            currentToken = &*it;
            FireParserError("macro.format") << "Macro abruptly ended (EOF-list)";
        }
        std::cout << "Defined macro with name=\"" << identifierDataCopy << "\" with contents(" << content.size()
                  << "):";
        for (const auto &t : content) std::cout << ' ' << t;
        std::cout << '\n';

        auto replaceIt = preprocessor.macroTable.find(identifierDataCopy);

        if (replaceIt != std::cend(preprocessor.macroTable)) {
            // replace the contents because it already exists
            replaceIt->second.tokens = std::move(content);
        } else {
            // append a new entry since it doesn't exist yet
            preprocessor.macroTable.insert({std::move(identifierDataCopy), {identifierTokenPtr, std::move(content)}});
        }
    }

    void Parser::CheckDefinableMacroIdentifier(const Unicode::UString &macroIdentifier,
                                               DefineAction action) {
        const auto builtinIt = std::find(std::cbegin(runtimeConstants.builtinMacros),
                                  std::cend(runtimeConstants.builtinMacros),
                                  macroIdentifier);

        if (builtinIt != std::cend(runtimeConstants.builtinMacros)) {
            switch (action) {
                case DefineAction::DEFINE:
                    FireParserError("directive.define.builtin") << "Re #define'ing a builtin macro: " << macroIdentifier;
                    break;
                case DefineAction::UNDEFINE:
                    FireParserError("directive.undef.builtin") << "#undef a builtin macro: " << macroIdentifier;
                    break;
                default:
                    break;
            }
        }

        if (warnings.macroIdentifierIsPredefined) {
            const auto predefineIt = predefinedMacros.find(macroIdentifier);

            if (predefineIt != std::cend(predefinedMacros)) {
                switch (action) {
                    case DefineAction::DEFINE:
                        FireWarning("directive.define.predefine")
                            << "Re #define'ing a predefined macro: "
                            << macroIdentifier;
                        break;
                    case DefineAction::UNDEFINE:
                        FireWarning("directive.undef.predefine")
                            << "#undef a predefined macro: "
                            << macroIdentifier;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    void Parser::ProcessUndefMacro(decltype(preprocessor.tokens)::iterator &identifier) {
        const Unicode::UString &identifierData = identifier->data;
        currentToken = &*identifier;

        CheckDefinableMacroIdentifier(identifierData, DefineAction::UNDEFINE);

        auto it = preprocessor.macroTable.find(identifierData);
        if (it != std::cend(preprocessor.macroTable)) {
            preprocessor.macroTable.erase(it);
        }
    }

    void Parser::ProcessPragmaDirective(decltype(preprocessor.tokens)::iterator &it,
                                        decltype(preprocessor.tokens) &tokens) {
        // simulate tokens as if they exist on macro expansions
        std::list<LinkedPreprocessorToken> pragmaTokens{};
        while (it != std::end(tokens)) {
            PreprocessorToken *token = &*it;

            if (it->type == PreprocessorTokenType::WHITESPACE) {
                if (it->data.contains(Unicode::LINE_FEED)) {
                    break;
                }
            } else if (it->type == PreprocessorTokenType::IDENTIFIER) {
                auto macroIt = preprocessor.macroTable.find(it->data);
                if (macroIt != std::end(preprocessor.macroTable)) {
                    for (auto &macroToken : macroIt->second.tokens) {
                        pragmaTokens.emplace_back(&macroToken, macroIt->second.token)
                            .firstMacroIdentifier = token;
                    }
                } else {
                    pragmaTokens.emplace_back(token);
                }
            } else {
                pragmaTokens.emplace_back(token);
            }

            ++it;
        }

        if (std::empty(pragmaTokens))
            return;

        if (pragmaTokens.front().effective->data == runtimeConstants.vendorGCC) {
            pragmaTokens.pop_front();
            ProcessGCCPragmaDirective(std::move(pragmaTokens));
            return;
        }
    }

    void
    Parser::HandleGenericInformationGCCPragma(
            std::list<LinkedPreprocessorToken> &&pragma,
            const Unicode::UString &type,
            auto &&operand) {

        // revocation is withdrawn at the end, when the #pragma directive
        // was parsed and validated.
        operand.revoked = true;

        if (std::empty(pragma)) {
            FireParserError("pragma.error") << "#pragma GCC " << type << " expects a quoted string, but nothing was given";
        }

        auto it = std::find_if_not(std::cbegin(pragma), std::cend(pragma),
            [](const LinkedPreprocessorToken &entry) {
                return entry.effective->type == PreprocessorTokenType::STRING_LITERAL;
            }
        );

        if (it != std::cend(pragma)) {
            currentToken = *it;
            FireParserError("pragma.error") << "#pragma GCC " << type << " expects a quoted string, but a " << it->effective->type << " was encountered";
        }

        for (const auto &token : pragma) {
            operand << token.effective->data;
        }

        // at this point, the #pragma directive is OK and parsed, so withdraw
        // the revocation status.
        operand.revoked = false;
    }

    void Parser::ProcessGCCPragmaDirective(std::list<LinkedPreprocessorToken> &&pragma) {
        if (std::empty(pragma))
            return;

        auto &typeToken = pragma.front();
        pragma.pop_front();

        if (typeToken.effective->data.equalsIgnoreCase(runtimeConstants.pragmaGCCPoison)) {
            for (const auto &token : pragma) {
                if (token.effective->type == PreprocessorTokenType::IDENTIFIER) {
                    pragmaOptions.poison.blockedList.push_back(token.effective->data);
                } else {
                    // TODO currentToken = &*it;
                    currentToken = typeToken;
                    FireParserError("pragma.poison") << "A " << *token.effective << " cannot be blacklisted, only identifiers";
                }
            }
        } else if (typeToken.effective->data.equalsIgnoreCase(runtimeConstants.pragmaGCCError)) {
            currentToken = typeToken;
            HandleGenericInformationGCCPragma(std::move(pragma), typeToken.effective->data, FireParserError("pragma.error"));
        } else if (typeToken.effective->data.equalsIgnoreCase(runtimeConstants.pragmaGCCWarning)) {
            currentToken = typeToken;
            HandleGenericInformationGCCPragma(std::move(pragma), typeToken.effective->data, FireWarning("pragma.warning"));
        }
    }

    void Parser::ReplaceEscapeSequences() {
        for (auto &token : preprocessor.tokens) {
            if (token.type != PreprocessorTokenType::STRING_LITERAL &&
                    token.type != PreprocessorTokenType::CHARACTER_CONSTANT) {
                continue;
            }

            currentToken = &token;

            auto it = std::begin(token.data);
            for (; it != std::end(token.data);) {
                ++currentToken.specificCharacter;

                if (*it != Unicode::REVERSE_SOLIDUS) {
                    ++it;
                    continue;
                }

                // TODO support other escape sequences than simple
                auto value = *(it + 1);
                ++currentToken.specificCharacter;
                Unicode::CodePoint replacement{Unicode::NULL_CHARACTER};

                switch (value) {
                    case Unicode::REVERSE_SOLIDUS:
                    case Unicode::APOSTROPHE:
                    case Unicode::QUOTATION_MARK:
                    case Unicode::QUESTION_MARK:
                        replacement = value;
                        break;
                    case Unicode::LATIN_SMALL_LETTER_A:
                    case Unicode::LATIN_SMALL_LETTER_B:
                        FireWarning("escape.legacy") << "This escape sequence is ignored";
                        break;
                    case Unicode::LATIN_SMALL_LETTER_F:
                        replacement = Unicode::FORM_FEED;
                        break;
                    case Unicode::LATIN_SMALL_LETTER_N:
                        replacement = Unicode::LINE_FEED;
                        break;
                    case Unicode::LATIN_SMALL_LETTER_R:
                        replacement = Unicode::CARRIAGE_RETURN;
                        break;
                    case Unicode::LATIN_SMALL_LETTER_T:
                        replacement = Unicode::CHARACTER_TABULATION;
                        break;
                    case Unicode::LATIN_SMALL_LETTER_V:
                        replacement = Unicode::LINE_TABULATION;
                        break;
                    default:
                        FireParserError("escape.unknown") << "Unknown escape sequence";
                }

                if (replacement == Unicode::NULL_CHARACTER) {
                    token.data.erase(it);
                } else {
                    *it = replacement;
                    ++it;
                }

                token.data.erase(it);
            }
        }
    }

    void Parser::ConcatenateAdjacentStringLiterals() {
        for (auto it = std::begin(preprocessor.tokens);
            it <= std::end(preprocessor.tokens) - 1;
        ) {
            if (it->type != PreprocessorTokenType::STRING_LITERAL) {
                ++it;
                continue;
            }

            auto startIter = it;
            std::size_t capacity{};

            for (auto iter = it; iter != std::end(preprocessor.tokens); ++iter) {
                if (iter->type == PreprocessorTokenType::WHITESPACE)
                    continue;
                if (iter->type != PreprocessorTokenType::STRING_LITERAL)
                    break;
                capacity += std::size(it->data);
            }

            if (capacity == std::size(startIter->data)) {
                ++it;
                // couldn't find an adjacent string literal
                std::cout << "[CASL] skip " << *startIter << '\n';
                continue;
            }

            std::vector<Unicode::CodePoint> buffer{};
            buffer.reserve(capacity);

            for (; it != std::end(preprocessor.tokens); ++it) {
                if (it->type == PreprocessorTokenType::WHITESPACE)
                    continue;
                if (it->type != PreprocessorTokenType::STRING_LITERAL)
                    break;
                std::copy(std::begin(it->data), std::end(it->data), std::back_inserter(buffer));
            }

            for (auto iter = startIter + 1; iter != it; ++iter) {
                std::cout << "erase=" << *iter << '\n';
            }

            preprocessor.tokens.erase(startIter + 1, it);
            startIter->data = Unicode::UString{std::move(buffer)};
        }
    }

    OperatorType
    Parser::TranslateOperatorType(const PreprocessorToken *token) {
        const std::size_t size = std::size(token->data);

        if (size == 1)
            return TranslateSingleOperatorType(token->data.front());

        Unicode::CodePoint nextCharacter = std::size(token->data) == 1 ?
                                           Unicode::NULL_CHARACTER :
                                           token->data[1];

        if (std::size(token->data) == 2) {
            if (nextCharacter == Unicode::EQUALS_SIGN)
                return TranslateOperatorTypeAfterEqualsSign(token->data.front());
        }

        FireParserError("operator") << "Unknown operator: " << token->data;
        return static_cast<OperatorType>(0);
    }

    OperatorType
    Parser::TranslateOperatorTypeAfterEqualsSign(Unicode::CodePoint character) {
        switch (character) {
            case Unicode::PLUS_SIGN:
                return OperatorType::ADDITION_ASSIGNMENT;
            case Unicode::HYPHEN_MINUS:
                return OperatorType::SUBTRACTION_ASSIGNMENT;
            case Unicode::SOLIDUS:
                return OperatorType::DIVISION_ASSIGNMENT;
            case Unicode::ASTERISK:
                return OperatorType::MULTIPLICATION_ASSIGNMENT;
            case Unicode::EQUALS_SIGN:
                return OperatorType::EQUAL_TO;
            default:
                FireParserError("operator-equals") << "Unknown operator: " << character << '=';
                return static_cast<OperatorType>(0);
        }
    }

    OperatorType
    Parser::TranslateSingleOperatorType(Unicode::CodePoint character) {
        switch (character) {
            case Unicode::PLUS_SIGN:
                return OperatorType::ADDITION;
            case Unicode::HYPHEN_MINUS:
                return OperatorType::SUBTRACTION;
            case Unicode::SOLIDUS:
                return OperatorType::SOLIDUS;
            case Unicode::ASTERISK:
                return OperatorType::ASTERISK;
            case Unicode::EQUALS_SIGN:
                return OperatorType::SIMPLE_ASSIGNMENT;
            default:
                FireParserError("operator-equals") << "Unknown operator: " << character << '=';
                return static_cast<OperatorType>(0);
        }
    }

    PunctuatorType
    Parser::TranslatePunctuatorType(const PreprocessorToken *token) {
        if (std::size(token->data) == 1) {
            switch (token->data.back()) {
                case Unicode::SEMICOLON:
                    return PunctuatorType::SEMICOLON;
                default:
                    break;
            }
        }

        FireParserError("punctuator") << "Unknown punctuator: " << token->data;
        return static_cast<PunctuatorType>(0);
    }

    void
    Parser::TranslateNumber(const PreprocessorToken *token) {
        if (!IsPPNumberSolelyDigits(token->data)) {
            FireParserError("number") << "Unsupported number";
            return;
        }

        std::string stdString(std::size(token->data), '?');
        for (std::size_t i = 0; i < std::size(token->data); ++i) {
            stdString[i] = static_cast<char>(token->data[i]);
        }

        std::size_t index = 0;
        std::uint64_t value = std::stoull(stdString, &index);

        if (std::size(stdString) != index) {
            FireParserError("number") << "Invalid number";
            return;
        }

        EmitParserToken(std::make_unique<UnsignedIntegerConstantToken>(value));
    }

    void Parser::TranslatePreprocessorTokensToParserTokens() {
        std::size_t curlyCount = 0;

        for (PreprocessorToken &preprocessorToken : preprocessor.tokens) {
            currentToken = &preprocessorToken;

            switch (preprocessorToken.type) {
                case PreprocessorTokenType::WHITESPACE:
                    // ignore - not significant
                    break;
                case PreprocessorTokenType::STRING_LITERAL:
                    EmitParserToken(std::make_unique<StringLiteralToken>(std::move(preprocessorToken.data)));
                    break;
                case PreprocessorTokenType::CHARACTER_CONSTANT:
                    if (std::size(preprocessorToken.data) != 1)
                        ParserError("character-constant.length") << "Character constant must only contain a single character!";
                    EmitParserToken(std::make_unique<CharacterConstantToken>(preprocessorToken.data.front()));
                    break;
                case PreprocessorTokenType::IDENTIFIER: {
                    auto it = runtimeConstants.keywordList.find(preprocessorToken.data);
                    if (it != std::cend(runtimeConstants.keywordList)) {
                        EmitParserToken(std::make_unique<KeywordToken>(it->second));
                    } else {
                        EmitParserToken(std::make_unique<IdentifierToken>(std::move(preprocessorToken.data)));
                    }
                } break;
                case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR:
                    if (preprocessorToken.data == Unicode::LEFT_CURLY_BRACKET) {
                       ++curlyCount;
                        EmitParserToken(std::make_unique<PunctuatorToken>(PunctuatorType::LEFT_CURLY_BRACKET));
                    } else if (preprocessorToken.data == Unicode::RIGHT_CURLY_BRACKET) {
                        if (curlyCount == 0) {
                            ParserError("block-end") << "Unexpected token";
                        } else {
                            --curlyCount;
                            EmitParserToken(std::make_unique<PunctuatorToken>(PunctuatorType::RIGHT_CURLY_BRACKET));
                        }
                    } else if (preprocessorToken.data == Unicode::LEFT_PARENTHESIS) {
                        if (curlyCount == 0) {
                            EmitParserToken(std::make_unique<PunctuatorToken>(PunctuatorType::LEFT_PARENTHESIS));
                        } else {
                            EmitParserToken(std::make_unique<OperatorToken>(OperatorType::LEFT_PARENTHESIS));
                        }
                    } else if (preprocessorToken.data == Unicode::RIGHT_PARENTHESIS) {
                        if (curlyCount == 0) {
                            EmitParserToken(std::make_unique<PunctuatorToken>(PunctuatorType::RIGHT_PARENTHESIS));
                        } else {
                            EmitParserToken(std::make_unique<OperatorToken>(OperatorType::RIGHT_PARENTHESIS));
                        }
                    } else if (preprocessorToken.data == Unicode::EQUALS_SIGN) {
                        // equal-sign punctuator, =, 3.1.6, 3.5, 3.5.7
                        // int a = 1;
                        // simple assignment operator, =, 3.3.16.1
                        // a = 2;
                        if (parserTokens.back()->type != ParserTokenType::IDENTIFIER) {
                            ParserError("equals-sign") << "Unexpected token, back=" << parserTokens.back()->type;
                            break;
                        }
                    } else if (preprocessorToken.data == Unicode::COMMA) {
                        // TODO a comma may also be an operator
                        EmitParserToken(std::make_unique<PunctuatorToken>(PunctuatorType::COMMA));
                    } else {
                        ParserError("unhandled token") << preprocessorToken;
                    }
                    break;
                case PreprocessorTokenType::OPERATOR:
                    EmitParserToken(std::make_unique<OperatorToken>(TranslateOperatorType(&preprocessorToken)));
                    break;
                case PreprocessorTokenType::PUNCTUATOR:
                    EmitParserToken(std::make_unique<PunctuatorToken>(TranslatePunctuatorType(&preprocessorToken)));
                    break;
                case PreprocessorTokenType::PP_NUMBER:
                    TranslateNumber(&preprocessorToken);
                    break;
                default:
                    ParserError("unhandled token") << preprocessorToken;
            }
        }
    }

} // namespace Parsing
