/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Text/UString.hpp"

namespace Parsing {

    struct PragmaOptions {

        /**
         * The GCC poison feature, reimplemented in this compiler
         * https://gcc.gnu.org/onlinedocs/cpp/Pragmas.html#index-_0023pragma-GCC-poison
         */
        struct Poison {
            enum class Level {
                ERROR,
                WARNING
            };

            std::vector<Unicode::UString> blockedList{};
            Level level{Level::ERROR};
        };

        Poison poison{};

    };

} // namespace Parsing
