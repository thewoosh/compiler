/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * Issue List:
 * (1) Missing operator<=> for std::reference_wrapper. I personally think this
 *     should be an obvious library feature. The problem is debated here:
 *     https://stackoverflow.com/q/10730111
 */

#pragma once

#include <functional> // for std::reference_wrapper

#include "Source/Text/UString.hpp"

namespace std {

    template<typename A, typename B>
    [[nodiscard]] auto
    operator<=>(const std::reference_wrapper<A> &a, const std::reference_wrapper<B> &b)
            noexcept(noexcept(a.get().operator<=>(b.get())))
            -> decltype(a.get().operator<=>(b.get())) {
        return a.get().operator<=>(b.get());
    }

    template<typename A, typename B>
    [[nodiscard]] auto
    operator<=>(const std::reference_wrapper<const A> &a, const std::reference_wrapper<const B> &b)
            noexcept(noexcept(a.get().operator<=>(b.get())))
            -> decltype(a.get().operator<=>(b.get())) {
        return a.get().operator<=>(b.get());
    }

} // namespace std
