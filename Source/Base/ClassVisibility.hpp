/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#if defined (COMPILER_DEBUGGING) || defined (COMPILER_TESTING)
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS public
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_STRUCTURES public
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES public
#else
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS private
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_STRUCTURES private
#   define CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES private
#endif
