/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 - 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <iterator>

#include "Stream.hpp"

namespace Data {

    /**
     * The Stream class is an abstraction for continuous resources. This class
     * provides an abstract interface for many different types of resources.
     * This way, a stream can get its resources either from disk, network memory
     * etc.
     */
    template<typename Unit, typename Iterator>
    class IteratorStream : public Stream<Unit> {
    public:
        constexpr
        IteratorStream(const Iterator &begin, const Iterator &end) noexcept
                : beginIterator(begin), currentIterator(beginIterator), endIterator(end) {
        }

        constexpr
        IteratorStream(const Iterator &begin, const Iterator &end, const Iterator &start) noexcept
            : beginIterator(begin), currentIterator(start), endIterator(end) {
        }

        /**
         * Returns whether or not the stream is past its elements.
         */
        [[nodiscard]] constexpr bool
        IsEOF() const noexcept override {
            return currentIterator >= endIterator;
        }

        /**
         * @see Stream::Peek
         */
        [[nodiscard]] constexpr bool
        IsReady() const noexcept override {
            return currentIterator < endIterator;
        }

        /**
         * @see Stream::Peek
         */
        [[nodiscard]] constexpr Unit
        Peek() const noexcept override {
            if (IsEOF()) {
                return static_cast<Unit>(0);
            }

            return *currentIterator;
        }

        /**
         * @see Stream::Read
         */
        [[nodiscard]] constexpr Unit
        Read() noexcept override {
            if (IsEOF()) {
                currentIterator = endIterator;
                return static_cast<Unit>(0);
            }

            return *currentIterator++;
        }

        /**
         * @see Stream::Reconsume
         */
        void
        Reconsume() noexcept override {
            if (currentIterator > beginIterator && currentIterator < endIterator)
                --currentIterator;
        }

    private:
        Iterator beginIterator;
        Iterator currentIterator;
        Iterator endIterator;
    };

} // namespace Data
