/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <cstring>

#include "UString.hpp"

namespace Unicode::FastUStrings {

    [[nodiscard]] constexpr inline bool
    CodePointEqualsChar(Unicode::CodePoint cp, char c) noexcept {
        return cp == static_cast<Unicode::CodePoint>(c);
    }

    [[nodiscard]] constexpr inline bool
    CodePointEqualsCharIgnoreCase(Unicode::CodePoint cp, char c) noexcept {
        return Unicode::ToLowerASCII(cp) == Unicode::ToLowerASCII(static_cast<Unicode::CodePoint>(c));
    }

    /**
     * Compare the UString to a C-string, fastly by interpreting the CodePoint
     * as ASCII <char>s.
     */
    [[nodiscard]] inline bool
    EqualsASCII(const Unicode::UString &a, const char *b) noexcept {
        std::size_t bSize = std::strlen(b);

        if (a.length() != bSize) {
            return false;
        }

        for (std::size_t i = 0; i < bSize; i++) {
            if (!CodePointEqualsChar(a[i], b[i])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compare the UString to a C-string, fastly by interpreting the CodePoint
     * as ASCII <char>s.
     */
    [[nodiscard]] inline bool
    EqualsASCIIIgnoreCase(const Unicode::UString &a,
                          const char *b) noexcept {
        std::size_t bSize = std::strlen(b);

        if (a.length() != bSize) {
            return false;
        }

        for (std::size_t i = 0; i < bSize; i++) {
            if (!CodePointEqualsCharIgnoreCase(a[i], b[i])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compare the UString to a C-string, but only the length of the C-string,
     * fastly by interpreting the CodePoint as ASCII <char>s.
     */
    [[nodiscard]] inline bool
    StartsWithASCIIIgnoreCase(const Unicode::UString &a,
                              const char *b) noexcept {
        std::size_t bSize = std::strlen(b);

        if (a.length() < bSize) {
            return false;
        }

        for (std::size_t i = 0; i < bSize; i++) {
            if (!CodePointEqualsCharIgnoreCase(a[i], b[i])) {
                return false;
            }
        }

        return true;
    }

} // namespace Unicode::FastUStrings
