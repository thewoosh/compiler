/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <functional>

#include "Source/Parsing/KeywordTokenType.hpp"
#include "Source/Text/UString.hpp"

namespace Parsing {

    struct RuntimeConstants {
        Unicode::UString directiveDefine{{'d', 'e', 'f', 'i', 'n', 'e'}};
        Unicode::UString directiveElif{{'e', 'l', 'i', 'f'}};
        Unicode::UString directiveElse{{'e', 'l', 's', 'e'}};
        Unicode::UString directiveEndif{{'e', 'n', 'd', 'i', 'f'}};
        Unicode::UString directiveError{{'e', 'r', 'r', 'o', 'r'}};
        Unicode::UString directiveIf{{'i', 'f'}};
        Unicode::UString directiveIfdef{{'i', 'f', 'd', 'e', 'f'}};
        Unicode::UString directiveIfndef{{'i', 'f', 'n', 'd', 'e', 'f'}};
        Unicode::UString directiveInclude{{'i', 'n', 'c', 'l', 'u', 'd', 'e'}};
        Unicode::UString directiveLine{{'l', 'i', 'n', 'e'}};
        Unicode::UString directivePragma{{'p', 'r', 'a', 'g', 'm', 'a'}};
        Unicode::UString directiveUndef{{'u', 'n', 'd', 'e', 'f'}};

        // Used for `#pragma CCC ...` detection.
        Unicode::UString vendorGCC{{'G', 'C', 'C'}};

        Unicode::UString pragmaGCCError{{'e', 'r', 'r', 'o', 'r'}};
        Unicode::UString pragmaGCCPoison{{'p', 'o', 'i', 's', 'o', 'n'}};
        Unicode::UString pragmaGCCWarning{{'w', 'a', 'r', 'n', 'i', 'n', 'g'}};

        Unicode::UString operatorSizeof{{'s', 'i', 'z', 'e', 'o', 'f'}};

        Unicode::UString fileNameBuiltin{"<built-in>"};

        Unicode::UString macroNameDate{"__DATE__"};
        Unicode::UString macroNameFile{"__FILE__"};
        Unicode::UString macroNameFunction{"__FUNCTION__"};
        Unicode::UString macroNameLine{"__LINE__"};
        Unicode::UString macroNameTime{"__TIME__"};
        Unicode::UString macroNameDefined{"defined"};

        std::array<std::reference_wrapper<const Unicode::UString>, 6> builtinMacros{{
            std::cref(macroNameDate),
            std::cref(macroNameFile),
            std::cref(macroNameFunction),
            std::cref(macroNameLine),
            std::cref(macroNameTime),
            std::cref(macroNameDefined)
        }};

        /* auto     double   int      struct
           break    else     long     switch
           case     enum     register typedef
           char     extern   return   union
           const    float    short    unsigned
           continue for      signed   void
           default  goto     sizeof   volatile
           do       if       static   while
        */
        const std::map<Unicode::UString, KeywordTokenType> keywordList = {
                {Unicode::UString{"auto"}, KeywordTokenType::AUTO },
                {Unicode::UString{"break"}, KeywordTokenType::BREAK },
                {Unicode::UString{"case"}, KeywordTokenType::CASE },
                {Unicode::UString{"char"}, KeywordTokenType::CHAR},
                {Unicode::UString{"const"}, KeywordTokenType::CONST},
                {Unicode::UString{"continue"}, KeywordTokenType::CONTINUE},
                {Unicode::UString{"default"}, KeywordTokenType::DEFAULT},
                {Unicode::UString{"do"}, KeywordTokenType::DO},
                {Unicode::UString{"double"}, KeywordTokenType::DOUBLE},
                {Unicode::UString{"else"}, KeywordTokenType::ELSE},
                {Unicode::UString{"enum"}, KeywordTokenType::ENUM},
                {Unicode::UString{"extern"}, KeywordTokenType::EXTERN},
                {Unicode::UString{"float"}, KeywordTokenType::FLOAT},
                {Unicode::UString{"for"}, KeywordTokenType::FOR},
                {Unicode::UString{"goto"}, KeywordTokenType::GOTO},
                {Unicode::UString{"if"}, KeywordTokenType::IF},
                {Unicode::UString{"int"}, KeywordTokenType::INT},
                {Unicode::UString{"long"}, KeywordTokenType::LONG},
                {Unicode::UString{"register"}, KeywordTokenType::REGISTER},
                {Unicode::UString{"return"}, KeywordTokenType::RETURN},
                {Unicode::UString{"short"}, KeywordTokenType::SHORT},
                {Unicode::UString{"signed"}, KeywordTokenType::SIGNED},
                {Unicode::UString{"sizeof"}, KeywordTokenType::SIZEOF},
                {Unicode::UString{"static"}, KeywordTokenType::STATIC},
                {Unicode::UString{"struct"}, KeywordTokenType::STRUCT},
                {Unicode::UString{"switch"}, KeywordTokenType::SWITCH},
                {Unicode::UString{"typedef"}, KeywordTokenType::TYPEDEF},
                {Unicode::UString{"union"}, KeywordTokenType::UNION},
                {Unicode::UString{"unsigned"}, KeywordTokenType::UNSIGNED},
                {Unicode::UString{"void"}, KeywordTokenType::VOID},
                {Unicode::UString{"volatile"}, KeywordTokenType::VOLATILE},
                {Unicode::UString{"while"}, KeywordTokenType::WHILE}
        };
    };

} // namespace Parsing
