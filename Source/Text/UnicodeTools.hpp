/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file contains tools that can be used in conjunction with the
 * Unicode::CodePoint environment.
 */

#pragma once

#include "Unicode.hpp"
#include "UnicodeCommons.hpp"

namespace Unicode {

    /**
     * Returns whether or not the supplied character is a letter in ASCII-space.
     *
     * Note that the ASCII-space defines letters for the Basic Latin alphabet,
     * and no other alphabets are recognized by this function.
     */
    [[nodiscard]] inline constexpr bool
    IsASCIIAlpha(CodePoint character) noexcept {
        return (character >= LATIN_CAPITAL_LETTER_A && character <= LATIN_CAPITAL_LETTER_Z)
            || (character >= LATIN_SMALL_LETTER_A   && character <= LATIN_SMALL_LETTER_Z);
    }

    /**
     * Returns whether or not the supplied character is a capital letter in
     * ASCII-space.
     *
     * Note that the ASCII-space defines letters for the Basic Latin alphabet,
     * and no other alphabets are recognized by this function.
     */
    [[nodiscard]] inline constexpr bool
    IsASCIIUpperAlpha(CodePoint character) {
        return character >= LATIN_CAPITAL_LETTER_A && character <= LATIN_CAPITAL_LETTER_Z;
    }

    /**
     * Returns whether or not the supplied character is a number.
     */
    [[nodiscard]] inline constexpr bool
    IsDigit(CodePoint character) noexcept {
        return character >= Unicode::DIGIT_ZERO && character <= Unicode::DIGIT_NINE;
    }

    /**
     * Returns whether or not the supplied character is a letter or a number in
     * ASCII-space.
     *
     * Note that the ASCII-space defines letters for the Basic Latin alphabet,
     * and no other alphabets are recognized by this function.
     */
    [[nodiscard]] inline constexpr bool
    IsASCIIAlphaNumeric(CodePoint character) noexcept {
        return IsDigit(character) || IsASCIIAlpha(character);
    }

    /**
     * Returns whether or not the supplied character is in the inclusive UTF-16
     * surrogate range.
     */
    [[nodiscard]] inline constexpr bool
    IsSurrogate(CodePoint character) noexcept {
        return character >= 0xD800 && character <= 0xDFFF;
    }

    /**
     * Returns the character lowercased if it is an ASCII capital letter,
     * otherwise returns the same character.
     */
    [[nodiscard]] inline constexpr CodePoint
    ToLowerASCII(CodePoint character) noexcept {
        /* Could use a ternary operator; this is cleaner. */
        if (character >= LATIN_CAPITAL_LETTER_A && character <= LATIN_CAPITAL_LETTER_Z)
            return character + 0x20;
        return character;
    }

    /**
     * Returns whether or not the supplied character is a C0 control.
     *
     * https://infra.spec.whatwg.org/#c0-control
     */
    [[nodiscard]] inline constexpr bool
    IsC0Control(CodePoint character) noexcept {
        return character <= Unicode::INFORMATION_SEPARATOR_ONE;
    }

    /**
     * Returns whether or not the given code point is inside the ASCII range.
     *
     * https://infra.spec.whatwg.org/#ascii-code-point
     */
    [[nodiscard]] inline constexpr bool
    IsASCIICodePoint(CodePoint codePoint) {
        return codePoint >= Unicode::NULL_CHARACTER && codePoint <= Unicode::DELETE;
    }

} // namespace Unicode
