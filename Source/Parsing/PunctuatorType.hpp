/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Parsing {

    enum class PunctuatorType {
        LEFT_PARENTHESIS,
        RIGHT_PARENTHESIS,
        LEFT_CURLY_BRACKET,
        RIGHT_CURLY_BRACKET,
        LEFT_SQUARE_BRACKET,
        RIGHT_SQUARE_BRACKET,
        ASTERISK,
        COMMA,
        COLON,
        EQUALS_SIGN,
        SEMICOLON,
        THREE_FULL_STOPS
    };

    [[nodiscard]] constexpr inline const char *
    TranslatePunctuatorTypeToStringLiteral(PunctuatorType type) noexcept {
        switch (type) {
            case PunctuatorType::LEFT_PARENTHESIS: return "left-parenthesis";
            case PunctuatorType::RIGHT_PARENTHESIS: return "right-parenthesis";
            case PunctuatorType::LEFT_CURLY_BRACKET: return "left-curly-bracket";
            case PunctuatorType::RIGHT_CURLY_BRACKET: return "right-curly-bracket";
            case PunctuatorType::LEFT_SQUARE_BRACKET: return "left-square-bracket";
            case PunctuatorType::RIGHT_SQUARE_BRACKET: return "right-square-bracket";
            case PunctuatorType::ASTERISK: return "asterisk";
            case PunctuatorType::COMMA: return "comma";
            case PunctuatorType::COLON: return "colon";
            case PunctuatorType::EQUALS_SIGN: return "equals-sign";
            case PunctuatorType::SEMICOLON: return "semicolon";
            case PunctuatorType::THREE_FULL_STOPS: return "three-full-stops";
            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, PunctuatorType type) {
        stream << TranslatePunctuatorTypeToStringLiteral(type);
        return stream;
    }

} // namespace Parsing
