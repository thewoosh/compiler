/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include <cassert>

#include "Source/SemanticAnalysis/SymbolStackFrame.hpp"

// constexpr vector is only supported from MSVC 16.9
#if defined (_MSC_VER) && _MSC_VER >= 1929
#   define CONSTEXPR_VECTOR constexpr
#else
#   define CONSTEXPR_VECTOR
#endif

namespace SemanticAnalysis {

    class SymbolStack {
        std::vector<SymbolStackFrame> frames{};

    public:
        [[nodiscard]] inline CONSTEXPR_VECTOR
        SymbolStack() noexcept
                : frames(1) {
        }

        inline CONSTEXPR_VECTOR void
        PushFrame() {
            frames.emplace_back();
        }

        inline CONSTEXPR_VECTOR void
        PopFrame() {
            // The first frame must be preserved.
            assert((std::size(frames) > 1));

            frames.pop_back();
        }

        /**
         * Finds a symbol with the name
         */
        [[nodiscard]] inline CONSTEXPR_VECTOR SymbolDescriptor *
        FindSymbol(const Unicode::UString &name) {
            const auto end = std::rend(frames);

            for (auto it = std::rbegin(frames); it != end; ++it) {
                if (auto *symbol = it->FindSymbol(name))
                    return symbol;
            }

            return nullptr;
        }

        inline CONSTEXPR_VECTOR void
        DefineSymbol(const Unicode::UString &name, SymbolDescriptor &&symbol) {
            frames.back().symbols.emplace(name, std::forward<SymbolDescriptor>(symbol));
        }
    };

} // namespace SemanticAnalysis
