/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include <cstdint>

#include "Source/AST/FunctionDeclarator.hpp"
#include "Source/CodeGeneration/ByteBuilder.hpp"

namespace CodeGeneration {

    struct FunctionData {
        const AST::FunctionDeclarator *declarator{};
        ByteBuilder builder{};
    };

} // namespace CodeGeneration
