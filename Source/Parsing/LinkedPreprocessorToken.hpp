/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Parsing/PreprocessorToken.hpp"

namespace Parsing {

    // When macros are expanded, we still want to preserve the token of the
    // macro-identifier that expanded the macro, to improve the
    // FormattableMessage
    //
    // The following example explains what these members mean:
    //
    // #define INTEGER_ONE 1
    //         ^^^^^^^^^^^ = source
    // #define SIZE_INTEGER_ONE
    //         ^^^^^^^^^^^^^^^^ = firstMacroIdentifier
    //
    // #pragma print-token SIZE_INTEGER_ONE
    //                     ^^^^^^^^^^^^^^^^ effective
    struct LinkedPreprocessorToken {

        // When simulated:
        // - the token that should be simulated
        // Otherwise:
        // - equal to source
        PreprocessorToken *effective;

        // When simulated:
        // - the token that expanded the macro
        // Otherwise:
        // - the token (equal to effective)
        PreprocessorToken *source;

        // A macro is often defined by a value that was expanded from another
        // macro. We want to display both, if needed.
        PreprocessorToken *firstMacroIdentifier{nullptr};

        std::size_t specificCharacter{0};

        [[nodiscard]] explicit constexpr inline
        operator bool() const noexcept {
            return source != nullptr;
        }

        [[nodiscard]] constexpr inline
        LinkedPreprocessorToken(PreprocessorToken *token) noexcept
                : effective(token)
                , source(token) {
        }

        [[nodiscard]] constexpr inline
        LinkedPreprocessorToken(PreprocessorToken *effective,
                                PreprocessorToken *source) noexcept
                : effective(effective)
                , source(source) {
        }

        LinkedPreprocessorToken(const LinkedPreprocessorToken &) noexcept = default;
    };

} // namespace Parsing
