/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace AST {

    enum class NodeType {
        ROOT,
        BLOCK,
        FUNCTION,
        STATEMENT_RETURN,
        STATEMENT_EXPRESSION,
    };

    [[nodiscard]] inline constexpr const char *
    TranslateNodeTypeToString(NodeType nodeType) noexcept {
        switch (nodeType) {
            case NodeType::ROOT: return "ROOT";
            case NodeType::BLOCK: return "BLOCK";
            case NodeType::FUNCTION: return "FUNCTION";
            case NodeType::STATEMENT_RETURN: return "STATEMENT_RETURN";
            case NodeType::STATEMENT_EXPRESSION: return "STATEMENT_EXPRESSION";
            default: return "(illegal value)";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, NodeType nodeType) {
        stream << TranslateNodeTypeToString(nodeType);
        return stream;
    }

} // namespace AST
