/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Text/Unicode.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace Parsing {

    [[nodiscard]] constexpr inline bool
    DoesCharacterEndIncludeStatement(Unicode::CodePoint character,
                                     Unicode::CodePoint includeCharacter
    ) noexcept {
        if (includeCharacter == Unicode::QUOTATION_MARK)
            return character == includeCharacter;
        if (includeCharacter == Unicode::LESS_THAN_SIGN)
            return character == Unicode::GREATER_THAN_SIGN;
        return false;
    }

    [[nodiscard]] constexpr inline bool
    IsIdentifierCharacter(Unicode::CodePoint character) noexcept {
        return character == Unicode::LOW_LINE ||
               Unicode::IsASCIIAlphaNumeric(character);
    }

    [[nodiscard]] constexpr inline bool
    IsOperatorOrPunctuator(Unicode::CodePoint character) {
        return character == Unicode::LEFT_SQUARE_BRACKET || character == Unicode::RIGHT_SQUARE_BRACKET || // [ ]
               character == Unicode::LEFT_PARENTHESIS    || character == Unicode::RIGHT_PARENTHESIS    || // ( )
               character == Unicode::LEFT_CURLY_BRACKET  || character == Unicode::RIGHT_CURLY_BRACKET  || // { }
               character == Unicode::LESS_THAN_SIGN      || character == Unicode::GREATER_THAN_SIGN    || // < >
               character == Unicode::EXCLAMATION_MARK    || character == Unicode::QUESTION_MARK        || // ! ?
               character == Unicode::HYPHEN_MINUS || character == Unicode::PLUS_SIGN ||                   // - +
               character == Unicode::EQUALS_SIGN  || character == Unicode::AMPERSAND ||                   // = &
               character == Unicode::SEMICOLON || character == Unicode::COLON ||                          // ; :
               character == Unicode::COMMA || character == Unicode::FULL_STOP ||                          // , .
               character == Unicode::NUMBER_SIGN || character == Unicode::TILDE ||                        // # ~
               character == Unicode::CIRCUMFLEX_ACCENT || character == Unicode::ASTERISK ||               // ^ *
               character == Unicode::PERCENTAGE_SIGN  || character == Unicode::VERTICAL_LINE ||           // % |
               character == Unicode::SOLIDUS                                                              // /
                ;
    }

    [[nodiscard]] constexpr inline Unicode::CodePoint
    TranslateTrigraph(Unicode::CodePoint character) {
        /**
         * ??=      #
         * ??(      [
         * ??/      \
         * ??)      ]
         * ??'      ^
         * ??<      {
         * ??!      |
         * ??>      }
         * ??-      ~
         */
        switch (character) {
            case Unicode::EQUALS_SIGN:
                return Unicode::NUMBER_SIGN;
            case Unicode::LEFT_PARENTHESIS:
                return Unicode::LEFT_SQUARE_BRACKET;
            case Unicode::SOLIDUS:
                return Unicode::REVERSE_SOLIDUS;
            case Unicode::RIGHT_PARENTHESIS:
                return Unicode::RIGHT_SQUARE_BRACKET;
            case Unicode::APOSTROPHE:
                return Unicode::CIRCUMFLEX_ACCENT;
            case Unicode::LESS_THAN_SIGN:
                return Unicode::LEFT_CURLY_BRACKET;
            case Unicode::EXCLAMATION_MARK:
                return Unicode::VERTICAL_LINE;
            case Unicode::GREATER_THAN_SIGN:
                return Unicode::RIGHT_CURLY_BRACKET;
            case Unicode::HYPHEN_MINUS:
                return Unicode::TILDE;
            default:
                return Unicode::NULL_CHARACTER;
        }
    }

    [[nodiscard]] constexpr inline bool
    IsWhitespaceCharacter(Unicode::CodePoint character) {
        return character == Unicode::SPACE ||
               character == Unicode::CHARACTER_TABULATION ||
               character == Unicode::LINE_TABULATION ||
               character == Unicode::FORM_FEED ||
               character == Unicode::LINE_FEED;
    }

    [[nodiscard]] inline std::vector<Unicode::CodePoint>
    CreateInternalDataForUString(std::size_t size) {
        std::vector<Unicode::CodePoint> vec(size, '\n');
        vec.clear();
        return vec;
    }

    [[nodiscard]] inline constexpr bool
    IsRepeatingCharacterAnOperator(Unicode::CodePoint character) noexcept {
        return character == Unicode::PLUS_SIGN      || // ++ increment
               character == Unicode::HYPHEN_MINUS   || // -- decrement
               character == Unicode::LESS_THAN_SIGN || // || bitwise or
               character == Unicode::NUMBER_SIGN;      // ## macro operator
    }

    [[nodiscard]] inline bool
    IsPPNumberSolelyDigits(const Unicode::UString &string) noexcept {
        for (Unicode::CodePoint codePoint : string)
            if (!Unicode::IsDigit(codePoint))
                return false;
        return true;
    }

} // namespace Parsing
