/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <ostream>

namespace Parsing {

    enum class KeywordTokenType {
        AUTO, BREAK, CASE, CHAR, CONST,
        CONTINUE, DEFAULT, DO, DOUBLE,
        ELSE, ENUM, EXTERN, FLOAT, FOR,
        GOTO, IF, INT, LONG, REGISTER,
        RETURN, SHORT, SIGNED, SIZEOF,
        STATIC, STRUCT, SWITCH, TYPEDEF,
        UNION, UNSIGNED, VOID, VOLATILE,
        WHILE
    };

    [[nodiscard]] constexpr inline const char *
    TranslateKeywordTokenTypeToStringLiteral(KeywordTokenType type) noexcept {
        switch (type) {
            case KeywordTokenType::AUTO: return "auto";
            case KeywordTokenType::BREAK: return "break";
            case KeywordTokenType::CASE: return "case";
            case KeywordTokenType::CHAR: return "char";
            case KeywordTokenType::CONST: return "const";
            case KeywordTokenType::CONTINUE: return "continue";
            case KeywordTokenType::DEFAULT: return "default";
            case KeywordTokenType::DO: return "do";
            case KeywordTokenType::DOUBLE: return "double";
            case KeywordTokenType::ELSE: return "else";
            case KeywordTokenType::ENUM: return "enum";
            case KeywordTokenType::EXTERN: return "extern";
            case KeywordTokenType::FLOAT: return "float";
            case KeywordTokenType::FOR: return "for";
            case KeywordTokenType::GOTO: return "goto";
            case KeywordTokenType::IF: return "if";
            case KeywordTokenType::INT: return "int";
            case KeywordTokenType::LONG: return "long";
            case KeywordTokenType::REGISTER: return "register";
            case KeywordTokenType::RETURN: return "return";
            case KeywordTokenType::SHORT: return "short";
            case KeywordTokenType::SIGNED: return "signed";
            case KeywordTokenType::SIZEOF: return "sizeof";
            case KeywordTokenType::STATIC: return "static";
            case KeywordTokenType::STRUCT: return "struct";
            case KeywordTokenType::SWITCH: return "switch";
            case KeywordTokenType::TYPEDEF: return "typedef";
            case KeywordTokenType::UNION: return "union";
            case KeywordTokenType::UNSIGNED: return "unsigned";
            case KeywordTokenType::VOID: return "void";
            case KeywordTokenType::VOLATILE: return "volatile";
            case KeywordTokenType::WHILE: return "while";
            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, KeywordTokenType type) {
        stream << TranslateKeywordTokenTypeToStringLiteral(type);
        return stream;
    }

} // namespace Parsing
