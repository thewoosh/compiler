/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <exception>

#include "Source/Base/Terminate.hpp"

namespace Encoding {

    /**
     * This enum defines the different types of errors the Encoder can
     * encounter.
     */
    enum class ErrorType {

        /**
         * The Encoder was given an integer that was outside of the Unicode
         * range - i.e. higher than U+10FFFF - making it *not* a code point.
         */
        NOT_AN_UNICODE_CODE_POINT,

        /**
         * The Encoder isn't ready yet as it isn't initialized (properly).
         */
        NOT_INITIALIZED,

        /**
         * The template<...UnitType...> is not supported.
         *
         * Generally, use std::uint8_t as UnitType, as it is the most controlled
         * and supported unit.
         */
        UNIT_NOT_SUPPORTED,

        /**
         * The end of the stream wasn't excepted, as more data is required to
         * parse the data.
         */
        UNEXPECTED_END_OF_STREAM,

        /**
         * UTF-8 doesn't allow bytes in some boundaries.
         *
         * TODO improve documentation?
         */
        UTF8_OUT_OF_BOUNDARIES,

        /**
         * UTF-8 doesn't allow bytes in some ranges.
         */
        UTF8_OUT_OF_RANGE,
    };

    class EncoderException
            : public std::exception {

        const char *
        what() const noexcept override {
            switch (errorType) {
                case ErrorType::NOT_AN_UNICODE_CODE_POINT:
                    return "NOT_AN_UNICODE_CODE_POINT";
                case ErrorType::NOT_INITIALIZED:
                    return "NOT_INITIALIZED";
                case ErrorType::UNIT_NOT_SUPPORTED:
                    return "UNIT_NOT_SUPPORTED";
                case ErrorType::UNEXPECTED_END_OF_STREAM:
                    return "UNEXPECTED_END_OF_STREAM";
                case ErrorType::UTF8_OUT_OF_BOUNDARIES:
                    return "UTF8_OUT_OF_BOUNDARIES";
                case ErrorType::UTF8_OUT_OF_RANGE:
                    return "UTF8_OUT_OF_RANGE";
                default: SWITCH_DEFAULT_UNREACHABLE();
            }
        }

    public:
        explicit
        EncoderException(ErrorType errorType)
            : errorType(errorType) {
        }

    private:
        ErrorType errorType;
    };

} // namespace Encoding
