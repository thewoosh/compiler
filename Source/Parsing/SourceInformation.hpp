/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint> // for std::size_t
#include <functional>

#include "Source/Text/UString.hpp"

namespace Parsing {

    struct SourceInformation {
        std::reference_wrapper<const Unicode::UString> fileName = std::cref(Unicode::EmptyString);
        std::size_t line{0};
        std::size_t column{0};
    };

} // namespace Parsing
