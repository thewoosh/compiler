/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map> // for std::map
#include <memory> // for std::shared_ptr, std::unique_ptr
#include <vector> // for std::vector

#include "Source/AST/Expression.hpp"
#include "Source/AST/FunctionDeclarator.hpp"
#include "Source/AST/NodeType.hpp"

namespace AST {

    struct Node {
        const NodeType type{};
        Node *const parentNode;

        std::vector<std::shared_ptr<Node>> children;

    protected:
        [[nodiscard]] inline
        Node(NodeType type, Node *parentNode)
                : type(type)
                , parentNode(parentNode) {
        }

        virtual ~Node() = default;
    };

    struct BlockNode : public Node {
        std::map<Unicode::UString, FunctionDeclarator> functionDeclarations{};

        [[nodiscard]] inline explicit
        BlockNode(Node *parentNode) : Node(NodeType::BLOCK, parentNode) {}

    protected:
        [[nodiscard]] inline
        BlockNode(NodeType type, Node *parentNode) : Node(type, parentNode) {}
    };

    struct RootNode : public BlockNode {
        [[nodiscard]] inline explicit
        RootNode() : BlockNode(NodeType::ROOT, nullptr) {}
    };

    struct FunctionNode : public BlockNode {
        const FunctionDeclarator *declarator{};

        [[nodiscard]] inline
        FunctionNode(Node *parentNode, const FunctionDeclarator *declarator)
                : BlockNode(NodeType::FUNCTION, parentNode)
                , declarator(declarator) {}
    };

    struct ReturnStatementNode : public Node {
        std::unique_ptr<Expression> expression;

        [[nodiscard]] inline explicit
        ReturnStatementNode(Node *parentNode, std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::STATEMENT_RETURN, parentNode)
                , expression(std::move(expression)) {
        }
    };

    struct ExpressionStatementNode : public Node {
        std::unique_ptr<Expression> expression{};

        [[nodiscard]] inline explicit
        ExpressionStatementNode(Node *parentNode, std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::STATEMENT_EXPRESSION, parentNode)
                , expression(std::move(expression)) {
        }
    };

} // namespace AST
