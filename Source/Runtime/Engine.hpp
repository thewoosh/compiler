/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>

#include <cstdint>

#include "Source/Base/ClassVisibility.hpp"
#include "Source/Text/UString.hpp"

namespace CodeGeneration { class Generator; }

namespace Runtime {

    class Engine {
    CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES:
        std::uint8_t *buffer{};
        std::size_t bufferSize{};
        const CodeGeneration::Generator *generator;

        std::map<Unicode::UString, const void *> functionTable{};

    CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS:
        [[nodiscard]] bool AllocateBuffer();
        [[nodiscard]] bool PlaceFunctions();

    public:
        [[nodiscard]] inline explicit
        Engine(const CodeGeneration::Generator *generator)
                : generator(generator) {
        }

        ~Engine() noexcept;

        int Run();
    };

} // namespace Runtime
