/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>
#include <string_view>

#include "Source/Parsing/SourceInformation.hpp"

namespace Parsing {

    class Parser;

    struct FormattableMessage {
        bool revoked{false};

        virtual ~FormattableMessage() = default;

        [[nodiscard]] virtual std::string_view
        Message() = 0;

        [[nodiscard]] virtual SourceInformation
        Source() = 0;

        [[nodiscard]] virtual std::string_view
        Tag() = 0;

        [[nodiscard]] virtual std::string_view
        TypeName() = 0;

        void
        PrintMessage(const Parser *, std::ostream &);
    };

}
