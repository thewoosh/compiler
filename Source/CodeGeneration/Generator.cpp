/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/CodeGeneration/Generator.hpp"

#include "Source/CodeGeneration/AssemblyGenerator.hpp"

namespace CodeGeneration {

    void Generator::Run() {
        for (const auto &node : rootNode->children) {
            if (node->type == AST::NodeType::FUNCTION) {
                HandleFunction(static_cast<const AST::FunctionNode *>(node.get()));
            } else {
                std::cout << "[CG] Expected FUNCTION node!\n";
                std::abort();
            }
        }
    }

    void Generator::HandleFunction(const AST::FunctionNode *node) {
        auto &function = functions.emplace(*node->declarator->name, FunctionData{node->declarator}).first->second;

        AssemblyGenerator generator{function};

        for (const auto &child : node->children) {
            if (child->type == AST::NodeType::STATEMENT_RETURN) {
                const auto &expression = static_cast<const AST::ReturnStatementNode *>(child.get())->expression;
                if (expression->type == AST::ExpressionType::PRIMARY_UNSIGNED_INTEGER_CONSTANT) {
                    generator.AddReturn(static_cast<const AST::PrimaryExpression *>(expression.get())->unsignedInteger);
                } else {
                    HandleExpression(expression.get(), &generator);
                    generator.AddReturn();
                }
            } else {
                std::cout << "[CG] Unexpected node in function!\n";
                std::abort();
            }
        }


        std::cout << "Code generated for function \"" << *function.declarator->name << std::hex << "\" (" << function.builder.size() << " octets)\n";
        for (std::size_t i = 0; i < generator.functionData.builder.size(); ++i) {
            std::cout << '(' << (i+1) << ") 0x" << static_cast<std::uint16_t>(generator.functionData.builder[i]) << '\n';
        }
        std::cout << std::dec << "========================\n";
    }

    void Generator::HandleExpression(const AST::Expression *expression,
                                     AssemblyGenerator *generator) {
        if (expression->type == AST::ExpressionType::ADDITIVE) {
            const auto *additiveExpression = static_cast<const AST::AdditiveExpression *>(expression);
            const auto *lhs = static_cast<const AST::PrimaryExpression *>(additiveExpression->left.get());
            const auto *rhs = static_cast<const AST::PrimaryExpression *>(additiveExpression->right.get());

            generator->SetAccumulator(lhs->unsignedInteger);
            if (additiveExpression->type == AST::AdditiveExpression::Type::ADDITION)
                generator->AccumulatorAdd(rhs->unsignedInteger);
            else
                generator->AccumulatorSubtract(rhs->unsignedInteger);

            return;
        }

        if (expression->type == AST::ExpressionType::POSTFIX_FUNCTION_CALL) {
            const auto *functionCall = static_cast<const AST::FunctionCallExpression *>(expression);
            if (functionCall->operand->type != AST::ExpressionType::PRIMARY_IDENTIFIER) {
                std::cout << "[CG] Non-dereference function calls are only supported, not a " << static_cast<std::uint16_t>(functionCall->operand->type) << '\n';
                std::abort();
            }

            // TODO push/store registers

            linkTable.emplace_back(
                    *static_cast<const AST::PrimaryExpression *>(functionCall->operand.get())->identifierName,
                    generator->AddCall()
            );
            return;
        }


        std::cout << "[CG] Unknown supported expression!\n";
        std::abort();
    }

} // namespace CodeGeneration

