/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>

namespace Algorithm {

    template <typename T>
    class HasContains {
    private:
        typedef char Yes;
        typedef Yes No[2];

        template<typename C, typename Param>
        static auto Test(const Param &param)
            -> decltype(bool{std::declval<C const>().contains(param)}, Yes{});

        template<typename> static No& Test(...);

    public:
        static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
    };


    template <typename T>
    class HasFind {
    private:
        typedef char Yes;
        typedef Yes No[2];

        template<typename C, typename Param> static auto Test(const Param &param)
        -> decltype(bool{std::declval<C const>().find(param)}, Yes{});

        template<typename> static No& Test(...);

    public:
        static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
    };

    template <typename Container, typename ContainerEntry>
    [[nodiscard]] constexpr inline auto
    Contains(const Container &container, const ContainerEntry &entry) noexcept
        -> decltype(container.contains(entry), void())
           requires(HasContains<Container>::value) {
        return container.contains(entry);
    }

    template <typename Key, typename Value>
    [[nodiscard]] constexpr inline bool
    Contains(const std::map<Key, Value> &map, const Key &key) noexcept {
        return map.find(key) != std::cend(map);
    }

    template <typename Container, typename ContainerEntry>
    [[nodiscard]] constexpr inline bool
    Contains(const Container &container, const ContainerEntry &entry) noexcept
        requires (std::is_same_v<decltype(*std::cbegin(container)), const ContainerEntry &>) {
        return std::find(std::cbegin(container), std::cend(container), entry)
                    != std::cend(container);
    }

} // namespace Algorithm
