/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Parsing {

    /*

[ ] ( ) . ->

++ -- & * + - ~ ! sizeof

/ % << >> < > <= >= == != ^ | && || ? :

= *= /= %= += -= <<= >>= &= ^= |= , # ##

    */

    enum class OperatorType {
        LEFT_PARENTHESIS,           // (
        RIGHT_PARENTHESIS,          // )
        LEFT_SQUARE_BRACKET,        // [
        RIGHT_SQUARE_BRACKET,       // ]
        FULL_STOP,                  // .
        POINTER_ACCESSOR,           // ->
        INCREMENT,                  // ++
        DECREMENT,                  // --

        AMPERSAND,                  // &
        ASTERISK,                   // *
        ADDITION,                   // +
        SUBTRACTION,                // -
        TILDE,                      // ~
        EXCLAMATION_MARK,           // !
        SIZEOF,                     // sizeof

        SOLIDUS,                    // /
        PERCENTAGE_SIGN,            // %
        BITWISE_SHIFT_LEFT,         // <<
        BITWISE_SHIFT_RIGHT,        // >>
        LESS_THAN,                  // <
        GREATER_THAN,               // >
        LESS_THAN_EQUAL,            // <=
        GREATER_THAN_EQUAL,         // >=
        EQUAL_TO,                   // ==
        NOT_EQUAL_TO,               // !=

        BITWISE_EXCLUSIVE_OR,       // ^
        BITWISE_INCLUSIVE_OR,       // |
        LOGICAL_AND,                // &&
        LOGICAL_OR,                 // ||
        QUESTION_MARK,              // ?
        COLON,                      // :
        SIMPLE_ASSIGNMENT,          // =

        MULTIPLICATION_ASSIGNMENT,  // *=
        DIVISION_ASSIGNMENT,        // /=
        REMAINDER_ASSIGNMENT,       // %=       modulo
        ADDITION_ASSIGNMENT,        // +=
        SUBTRACTION_ASSIGNMENT,     // -=
        LEFT_SHIFT_ASSIGNMENT,      // <<=
        RIGHT_SHIFT_ASSIGNMENT,     // >>=
        AMPERSAND_ASSIGNMENT,       // &=
        EXCLUSIVE_OR_ASSIGNMENT,    // ^=
        INCLUSIVE_OR_ASSIGNMENT,    // |=
        COMMA,                      // ,

        // # and ## are only for macro expansions

    };

    [[nodiscard]] constexpr inline const char *
    TranslateOperatorTypeToStringLiteral(OperatorType type) noexcept {
        switch (type) {
            case OperatorType::LEFT_PARENTHESIS: return "left-parenthesis(()";
            case OperatorType::RIGHT_PARENTHESIS: return "right-parenthesis())";
            case OperatorType::LEFT_SQUARE_BRACKET: return "left-square-bracket([)";
            case OperatorType::RIGHT_SQUARE_BRACKET: return "right-square-bracket(])";
            case OperatorType::FULL_STOP: return "full-stop(.)";
            case OperatorType::POINTER_ACCESSOR: return "pointer-accessor(->)";
            case OperatorType::INCREMENT: return "increment(++)";
            case OperatorType::DECREMENT: return "decrement(--)";

            case OperatorType::AMPERSAND: return "ampersand(&)";
            case OperatorType::ASTERISK: return "asterisk(*)";
            case OperatorType::ADDITION: return "addition(+)";
            case OperatorType::SUBTRACTION: return "subtraction(-)";
            case OperatorType::TILDE: return "tilde(~)";
            case OperatorType::EXCLAMATION_MARK: return "exclamation-mark(!)";
            case OperatorType::SIZEOF: return "sizeof(sizeof)";

            case OperatorType::SOLIDUS: return "solidus(/)";
            case OperatorType::PERCENTAGE_SIGN: return "percentage-sign(%)";
            case OperatorType::BITWISE_SHIFT_LEFT: return "bitwise-shift-left(<<)";
            case OperatorType::BITWISE_SHIFT_RIGHT: return "bitwise-shift-right(>>)";
            case OperatorType::LESS_THAN: return "less-than(<)";
            case OperatorType::GREATER_THAN: return "greater-than(<)";
            case OperatorType::LESS_THAN_EQUAL: return "less-than-equal(<=)";
            case OperatorType::GREATER_THAN_EQUAL: return "greater-than-equal(<=)";
            case OperatorType::EQUAL_TO: return "equal-to(==)";
            case OperatorType::NOT_EQUAL_TO: return "not-equal-to(!=)";

            case OperatorType::BITWISE_EXCLUSIVE_OR: return "bitwise-exclusive-or(^)";
            case OperatorType::BITWISE_INCLUSIVE_OR: return "bitwise-inclusive-or(|)";
            case OperatorType::LOGICAL_AND: return "logical-and(&&)";
            case OperatorType::LOGICAL_OR: return "logical-or(||)";
            case OperatorType::QUESTION_MARK: return "question-mark(?)";
            case OperatorType::COLON: return "colon(:)";
            case OperatorType::SIMPLE_ASSIGNMENT: return "simple-assignment(=)";

            case OperatorType::MULTIPLICATION_ASSIGNMENT: return "multiplication-assignment(*=)";
            case OperatorType::DIVISION_ASSIGNMENT: return "division-assignment(/=)";
            case OperatorType::REMAINDER_ASSIGNMENT: return "remainder-assignment(%=)";
            case OperatorType::ADDITION_ASSIGNMENT: return "addition-assignment(+=)";
            case OperatorType::SUBTRACTION_ASSIGNMENT: return "subtraction-assignment(-=)";
            case OperatorType::LEFT_SHIFT_ASSIGNMENT: return "left-shift-assignment(<<=)";
            case OperatorType::RIGHT_SHIFT_ASSIGNMENT: return "right-shift-assignment(>>=)";
            case OperatorType::AMPERSAND_ASSIGNMENT: return "ampersand-assignment(&=)";
            case OperatorType::EXCLUSIVE_OR_ASSIGNMENT: return "exclusive-or-assignment(^=)";
            case OperatorType::INCLUSIVE_OR_ASSIGNMENT: return "inclusive-or-assignment(|=)";

            case OperatorType::COMMA: return "comma(,)";

            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, OperatorType type) {
        stream << TranslateOperatorTypeToStringLiteral(type);
        return stream;
    }

} // namespace Parsing
