/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>
#include <utility>

#include "Source/Parsing/SourceInformation.hpp"

namespace Parsing {

    enum class PreprocessorTokenType {
        HEADER_NAME,
        IDENTIFIER,
        PP_NUMBER,
        CHARACTER_CONSTANT,
        STRING_LITERAL,
        OPERATOR,
        PUNCTUATOR,

        OTHER,
        WHITESPACE,
        PUNCTUATOR_OR_OPERATOR
    };

    [[nodiscard]] constexpr inline const char *
    TranslatePreprocessorTokenTypeToStringLiteral(PreprocessorTokenType type) {
        switch (type) {
            case PreprocessorTokenType::HEADER_NAME: return "header-name";
            case PreprocessorTokenType::IDENTIFIER: return "identifier";
            case PreprocessorTokenType::PP_NUMBER: return "pp-number";
            case PreprocessorTokenType::CHARACTER_CONSTANT: return "character-constant";
            case PreprocessorTokenType::STRING_LITERAL: return "string-literal";
            case PreprocessorTokenType::OPERATOR: return "operator";
            case PreprocessorTokenType::PUNCTUATOR: return "punctuator";
            case PreprocessorTokenType::OTHER: return "i-other";
            case PreprocessorTokenType::WHITESPACE: return "i-white-space";
            case PreprocessorTokenType::PUNCTUATOR_OR_OPERATOR: return "i-punctuator-or-operator";
            default: return nullptr;
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, PreprocessorTokenType type) {
        stream << TranslatePreprocessorTokenTypeToStringLiteral(type);
        return stream;
    }

    struct PreprocessorToken {

        SourceInformation sourceInformation;

        PreprocessorTokenType type;
        Unicode::UString data{};

        bool final{false};

        PreprocessorToken(SourceInformation &&info,
                          PreprocessorTokenType type) noexcept
                : sourceInformation(info)
                , type(type)
        {}

        PreprocessorToken(SourceInformation &&info,
                          PreprocessorTokenType type,
                          Unicode::UString &&data) noexcept
                : sourceInformation(info)
                , type(type)
                , data(std::move(data))
        {}

        template <typename... Args>
        explicit
        PreprocessorToken(SourceInformation &&info,
                          PreprocessorTokenType type,
                          Args&&... args) noexcept
                : sourceInformation(info)
                , type(type)
                , data(std::forward<Args>(args)...)
        {}

        PreprocessorToken(const PreprocessorToken &) = default;


    };

    inline std::ostream &
    operator<<(std::ostream &stream, const PreprocessorToken &token) {
        stream << "PreprocessorToken{type=" << token.type << ", data=";

        if (token.data.length() == 0) {
            stream << "empty";
        } else if (token.data.length() == 1) {
            switch (token.data.back()) {
                case Unicode::FORM_FEED:
                    stream << "'\\f'";
                    break;
                case Unicode::LINE_FEED:
                    stream << "'\\n'";
                    break;
                case Unicode::CARRIAGE_RETURN:
                    stream << "'\\r'";
                    break;
                case Unicode::CHARACTER_TABULATION:
                    stream << "'\\t'";
                    break;
                case Unicode::LINE_TABULATION:
                    stream << "'\\v'";
                    break;
                default:
                    stream << '\'' << static_cast<char>(token.data.back()) << '\'';
                    break;
            }
        } else {
            stream << '"' << token.data << '"';
        }

        if (token.final)
            stream << ", final";

        stream << '}';

        return stream;
    }

} // namespace Parsing
