/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <memory>
#include <string>
#include <string_view>
#include <utility>

#include "Source/Parsing/FormattableMessage.hpp"
#include "Source/Parsing/ParserToken.hpp"
#include "Source/Parsing/PreprocessorToken.hpp"
#include "Source/Parsing/SourceInformation.hpp"

namespace AST {

    struct ConstructionException
            : public std::exception
            , public Parsing::FormattableMessage {

        std::string_view tag;
        const std::unique_ptr<Parsing::ParserToken> &token;
        std::string data{};

        ConstructionException(std::string_view tag, const std::unique_ptr<Parsing::ParserToken> &token)
                : tag(tag)
                , token(token) {
        }

        ConstructionException(const ConstructionException &constructionException) noexcept
                : tag(constructionException.tag)
                , token(constructionException.token)
                , data(constructionException.data) {
        }

        [[nodiscard]] const char *
        what() const noexcept override {
            return data.c_str();
        }

        [[nodiscard]] std::string_view
        Message() override {
            return data;
        }

        [[nodiscard]] Parsing::SourceInformation
        Source() override {
            return token->associatedPreprocessorToken->sourceInformation;
        }

        [[nodiscard]] std::string_view
        Tag() override {
            return tag;
        }

        [[nodiscard]] std::string_view
        TypeName() override {
            return "parser error";
        }

    };

} // namespace AST
