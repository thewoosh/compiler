/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <vector>

#include "Source/AST/Node.hpp"
#include "Source/Base/ClassVisibility.hpp"
#include "Source/CodeGeneration/FunctionData.hpp"
#include "Source/Text/UString.hpp"

namespace CodeGeneration {

    struct AssemblyGenerator;

    struct LinkEntry {
        const Unicode::UString &functionName;
        std::uint8_t *ptr;

        [[nodiscard]] inline
        LinkEntry(const Unicode::UString &functionName, std::uint8_t *ptr) noexcept
                : functionName(functionName), ptr(ptr) {
        }
    };

    class Generator {
    CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES:
        const AST::Node *rootNode;
        std::map<Unicode::UString, FunctionData> functions{};

        std::vector<LinkEntry> linkTable{};

    CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS:
        void HandleFunction(const AST::FunctionNode *);
        void HandleExpression(const AST::Expression *, AssemblyGenerator *);

    public:
        [[nodiscard]] inline explicit
        Generator(const AST::Node *rootNode) noexcept
                : rootNode(rootNode) {
        }

        void Run();

        [[nodiscard]] inline const auto &
        Functions() const noexcept {
            return functions;
        }
    };

} // namespace CodeGeneration
