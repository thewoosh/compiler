/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <memory>
#include <vector>

#include "Source/AST/ArgumentList.hpp"
#include "Source/AST/FunctionDeclarator.hpp"
#include "Source/AST/Node.hpp"
#include "Source/AST/ParserState.hpp"
#include "Source/Base/ClassVisibility.hpp"
#include "Source/Parsing/ParserToken.hpp"
#include "Source/Parsing/PreprocessorToken.hpp"

namespace AST {

    class TreeConstructor {
        using TokenVectorType = std::vector<std::unique_ptr<Parsing::ParserToken>>;
        using TokensIterator = TokenVectorType::const_iterator;

    CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES:
        const TokenVectorType &tokens;
        TokensIterator tokenIterator;

        RootNode rootNode{};
        BlockNode *currentBlockNode{std::addressof(rootNode)};
        ParserState parserState{ParserState::INITIAL};

        const Parsing::ParserToken *typeToken{nullptr};
        const Parsing::ParserToken *identifierToken{nullptr};
        ArgumentList argumentList{};

    CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS:
        void InvokeParserError(std::string_view);

        //
        // Processor state implementations
        //
#define PARSER_STATE_ELEMENT(element) void ProcessState_##element(const Parsing::ParserToken *);
        PARSER_STATE_FOREACH()
#undef  PARSER_STATE_ELEMENT

        /**
         * Get the function we're currently in. Returns nullptr if we aren't
         * inside one.
         */
        [[nodiscard]] FunctionNode *GetEnclosingFunction();
        [[nodiscard]] std::unique_ptr<Expression> ConsumeExpression();
        void ConsumeSemiColon();
        void ConsumeFunctionCall();

        [[nodiscard]] std::unique_ptr<Expression> ConsumePrimaryExpression();
        [[nodiscard]] std::unique_ptr<Expression> ConsumePostfixExpression();
        [[nodiscard]] std::unique_ptr<Expression> ConsumeAdditiveExpression();

    public:
        [[nodiscard]] explicit inline
        TreeConstructor(const auto &tokens) noexcept
            : tokens(tokens) {
        }

        void Run();

        [[nodiscard]] inline constexpr RootNode *
        GetRootNode() noexcept { return &rootNode; }

        [[nodiscard]] inline constexpr const RootNode *
        GetRootNode() const noexcept { return &rootNode; }
    };

} // namespace AST
