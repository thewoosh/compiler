/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/AST/Node.hpp"
#include "Source/SemanticAnalysis/SymbolStack.hpp"
#include "Source/Settings/SettingsTable.hpp"

namespace SemanticAnalysis {

    class Analyzer {
        const AST::RootNode *rootNode;
        const Settings::SettingsTable &settingsTable;

        const AST::FunctionNode *functionPtr{nullptr};

        SymbolStack symbolStack{};

        [[nodiscard]] bool InspectNode(const AST::Node *);
        [[nodiscard]] bool InspectFunction(const AST::FunctionNode *);
        [[nodiscard]] bool InspectReturnStatement(const AST::ReturnStatementNode *);

    public:
        [[nodiscard]] inline
        Analyzer(const AST::RootNode *rootNode,
                 const Settings::SettingsTable &settingsTable) noexcept
                : rootNode(rootNode)
                , settingsTable(settingsTable) {
            static_cast<void>(this->settingsTable);
        }

        [[nodiscard]] bool
        Invoke();
    };

} // namespace SemanticAnalysis
