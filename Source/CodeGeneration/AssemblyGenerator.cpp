/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/CodeGeneration/AssemblyGenerator.hpp"

namespace CodeGeneration {

    void AssemblyGenerator::AddReturn() {
        functionData.builder.AppendOctet(0xc3);
    }

    void AssemblyGenerator::AddReturn(std::uint32_t value) {
        SetAccumulator(value);
        AddReturn();
    }

    void AssemblyGenerator::SetAccumulator(std::uint32_t value) {
        if (value == 0) {
            functionData.builder.AppendOctet(0x31); // xor eax,
            functionData.builder.AppendOctet(0xc0); // eax
        } else {
            functionData.builder.AppendOctet(0xb8); // mov eax,
            functionData.builder.AppendDword(value); // <value>
        }
    }

    void AssemblyGenerator::AccumulatorAdd(std::uint32_t value) {
        functionData.builder.AppendOctet(0x83);
        functionData.builder.AppendOctet(0xc0);
        functionData.builder.AppendOctet(value & 0xFF);
        // TODO support bigger types
    }

    void AssemblyGenerator::AccumulatorSubtract(std::uint32_t value) {
        functionData.builder.AppendOctet(0x05); // add eax, <val>
        functionData.builder.AppendDword(-value);
    }

    std::uint8_t *
    AssemblyGenerator::AddCall() {
        functionData.builder.AppendOctet(0xe8);
        auto *const pos = std::data(functionData.builder) + std::size(functionData.builder);
        functionData.builder.AppendDword(0);
        return pos;
    }

} // namespace CodeGeneration

