/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <functional> // for std::reference_wrapper
#include <map> // for std::map

#include "Source/Base/MissingStandardLibraryFeatures.hpp"
#include "Source/SemanticAnalysis/SymbolDescriptor.hpp"
#include "Source/Text/UString.hpp"

namespace SemanticAnalysis {

    struct SymbolStackFrame {

        std::map<std::reference_wrapper<const Unicode::UString>, SymbolDescriptor> symbols{};

        [[nodiscard]] inline /* constexpr */ SymbolDescriptor *
        FindSymbol(const Unicode::UString &name) noexcept {
            const auto it = symbols.find(name);

            if (it == std::end(symbols))
                return nullptr;

            return std::addressof(it->second);
        }

    };

} // namespace SemanticAnalysis
