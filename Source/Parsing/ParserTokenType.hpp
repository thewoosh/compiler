/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace Parsing {

    enum class ParserTokenType {
        CONSTANT_CHARACTER,
        CONSTANT_ENUMERATION,
        CONSTANT_FLOATING,
        CONSTANT_SIGNED_INTEGER,
        CONSTANT_UNSIGNED_INTEGER,
        IDENTIFIER,
        KEYWORD,
        OPERATOR,
        PUNCTUATOR,
        STRING_LITERAL
    };

    [[nodiscard]] constexpr inline const char *
    TranslateParserTokenTypeToStringLiteral(ParserTokenType type) noexcept {
        switch (type) {
            case ParserTokenType::KEYWORD:
                return "keyword";
            case ParserTokenType::IDENTIFIER:
                return "identifier";
            case ParserTokenType::CONSTANT_CHARACTER:
                return "constant-character";
            case ParserTokenType::CONSTANT_ENUMERATION:
                return "constant-enumeration";
            case ParserTokenType::CONSTANT_FLOATING:
                return "constant-floating";
            case ParserTokenType::CONSTANT_SIGNED_INTEGER:
                return "constant-signed-integer";
            case ParserTokenType::CONSTANT_UNSIGNED_INTEGER:
                return "constant-unsigned-integer";
            case ParserTokenType::STRING_LITERAL:
                return "string-literal";
            case ParserTokenType::OPERATOR:
                return "operator";
            case ParserTokenType::PUNCTUATOR:
                return "punctuator";
            default:
                return "illegal-value";
       }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, ParserTokenType type) {
        stream << TranslateParserTokenTypeToStringLiteral(type);
        return stream;
    }

} // namespace Parsing
