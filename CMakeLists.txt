# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>

cmake_minimum_required(VERSION 3.13)

project(Compiler
        HOMEPAGE_URL https://gitlab.com/thewoosh/compiler
        VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

include_directories(.)
add_compile_options(-Wall -Wextra -Werror -pedantic -Wformat=2)

add_subdirectory(Source)

# Download and unpack googletest at configure time
configure_file(ThirdParty/GoogleTest/CMakeLists.in.txt ThirdParty/GoogleTest/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ThirdParty/GoogleTest )
if(result)
    message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ThirdParty/GoogleTest )
if(result)
    message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
#
# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
        ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
        EXCLUDE_FROM_ALL)

enable_testing()
add_subdirectory("Testing")
