/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Parsing/FormattableMessage.hpp"
#include "Source/Parsing/Parser.hpp"

namespace Parsing {

    void PrintLocation(std::ostream &stream,
                       const SourceInformation *info,
                       std::string_view message) {
        stream << info->fileName << ':'
               << (info->line-1) << ':'
               << (info->column+1) << ':'
               << ' ' << message << '\n';
    }

    void PrintFilePortion(std::ostream &stream,
                          const Parser *parser,
                          const SourceInformation *info,
                          std::size_t portionSize,
                          std::size_t offset = 0) {
        stream << parser->lines[info->line - 1] << '\n'
               << std::string(info->column + offset, ' ')
               << std::string(portionSize, '^')
               << '\n';
    }

    void FormattableMessage::PrintMessage(const Parser *parser,
                                          std::ostream &stream) {
        if (revoked)
            return;

        SourceInformation ourSource = Source();

        const Parsing::SourceInformation *info = &ourSource;
        std::size_t portionSize = 1;
        std::size_t offset = 0;

        // TODO do something better with LinkedPreprocessorToken, like showing
        //      the macro expansion
        if (parser->currentToken) {
            if (parser->currentToken.firstMacroIdentifier) {
                info = &parser->currentToken.firstMacroIdentifier->sourceInformation;
                portionSize = parser->currentToken.firstMacroIdentifier->data.length();
            } else {
                info = &parser->currentToken.source->sourceInformation;
                if (parser->currentToken.specificCharacter == 0)
                    portionSize = parser->currentToken.source->data.length();
                else {
                    portionSize = 1;
                    offset = parser->currentToken.specificCharacter;
                }
            }
        }

        PrintLocation(stream, info, this->TypeName());

        stream << this->Tag() << ": "
               << this->Message() << '\n';

        PrintFilePortion(stream, parser, info, portionSize, offset);


        if (parser->currentToken.source != parser->currentToken.effective) {
            std::string_view message{"previously defined here"};

            if (parser->currentToken.firstMacroIdentifier) {
                info = &parser->currentToken.source->sourceInformation;
                portionSize = parser->currentToken.source->data.length();

                PrintLocation(stream, info, message);
                PrintFilePortion(stream, parser, info, portionSize);

                message = "expanded from a macro defined here";
            }

            info = &parser->currentToken.effective->sourceInformation;
            portionSize = parser->currentToken.effective->data.length();

            PrintLocation(stream, info, message);
            PrintFilePortion(stream, parser, info, portionSize);
        }
    }

} // namespace Parsing
