/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "TreeConstructor.hpp"

#include <cassert>

#include <iostream>
#include <stdexcept>

#define TC_TODO(reason) \
        std::cout << "TODO> ParserState: " << parserState << '\n'; \
        std::cout << "TODO> Token type: " << token->type << '\n'; \
        throw std::runtime_error("TODO: " reason);

#define TC_ADD_STATE(name) \
    case ParserState::name: \
        ProcessState_##name(token.get()); \
        break;

namespace AST {

    void PrintNode(const Node *node, std::size_t depth=0) {
        std::cout << std::string(depth * 2, ' ') << node->type << " (" << std::size(node->children) << ")\n";

        for (const auto &child : node->children)
            PrintNode(child.get(), depth + 1);
    }

    void
    TreeConstructor::Run() {
        std::cout << "[TreeConstructor]\n";

        for (tokenIterator = std::cbegin(tokens);
             tokenIterator != std::cend(tokens);
             ++tokenIterator) {
            const auto &token = *tokenIterator;

            std::cout << "  " << parserState << " => " << token->type << "\n";

            switch (parserState) {
                #define PARSER_STATE_ELEMENT(element) TC_ADD_STATE(element)
                PARSER_STATE_FOREACH()
                #undef  PARSER_STATE_ELEMENT
                default: TC_TODO("ParserState not implemented")
            }
        }

        PrintNode(&rootNode);
    }

    void TreeConstructor::InvokeParserError(std::string_view message) {
        std::cout << "Parser Error:\n";
        std::cout << "  Message: " << message << '\n';

        auto type = (**tokenIterator).type;
        std::cout << "  Token Type: " << type << '\n';

        if (type == Parsing::ParserTokenType::PUNCTUATOR)
            std::cout << "  Value: " << tokenIterator->get()->As<Parsing::PunctuatorToken>()->value << '\n';
        if (type == Parsing::ParserTokenType::OPERATOR)
            std::cout << "  Value: " << tokenIterator->get()->As<Parsing::OperatorToken>()->value << '\n';
        if (type == Parsing::ParserTokenType::KEYWORD)
            std::cout << "  Value: " << tokenIterator->get()->As<Parsing::KeywordToken>()->value << '\n';

        std::abort();
    }

    void TreeConstructor::ProcessState_INITIAL(const Parsing::ParserToken *token) {
        if (token->type == Parsing::ParserTokenType::KEYWORD) {
            auto val = token->As<Parsing::KeywordToken>()->value;

            if (val == Parsing::KeywordTokenType::INT) {
                typeToken = token;
                parserState = ParserState::AFTER_TYPE;
                return;
            }

            if (val == Parsing::KeywordTokenType::RETURN) {
                auto *func = GetEnclosingFunction();
                if (!func) {
                    InvokeParserError("return statement used outside a function");
                    return;
                }

                ++tokenIterator;
                auto expression = ConsumeExpression();
                if (!expression) {
                    return;
                }

                currentBlockNode->children.push_back(
                        std::make_unique<ReturnStatementNode>(currentBlockNode, std::move(expression))
                );

                ConsumeSemiColon();
                --tokenIterator; // because Run() increments it after this function
                return;
            }

            if (token->type == Parsing::ParserTokenType::IDENTIFIER) {
                parserState = ParserState::AFTER_IDENTIFIER;
                identifierToken = token;
                return;
            }

            std::cout << "initial has keyword of type: " << val << '\n';
        }

        if (token->type == Parsing::ParserTokenType::PUNCTUATOR) {
            auto val = token->As<Parsing::PunctuatorToken>()->value;

            if (val == Parsing::PunctuatorType::RIGHT_CURLY_BRACKET) {
                assert(currentBlockNode->parentNode);
                assert(currentBlockNode->parentNode->type == NodeType::BLOCK
                       || currentBlockNode->parentNode->type == NodeType::ROOT
                       || currentBlockNode->parentNode->type == NodeType::FUNCTION);

                currentBlockNode = static_cast<BlockNode *>(currentBlockNode->parentNode);
                return;
            }

            std::cout << "initial has punctuator " << val << '\n';
        }

        if (token->type == Parsing::ParserTokenType::IDENTIFIER) {
            identifierToken = token;
            parserState = ParserState::AFTER_IDENTIFIER;
            return;
        }

        TC_TODO("unrecognized token")
    }

    void TreeConstructor::ProcessState_AFTER_TYPE(const Parsing::ParserToken *token) {
        if (token->type == Parsing::ParserTokenType::IDENTIFIER) {
            parserState = ParserState::AFTER_TYPE_IDENTIFIER;
            identifierToken = token;
            return;
        }

        TC_TODO("unrecognized token")
    }

    void TreeConstructor::ProcessState_AFTER_TYPE_IDENTIFIER(const Parsing::ParserToken *token) {
        if (token->type == Parsing::ParserTokenType::PUNCTUATOR) {
            auto val = token->As<Parsing::PunctuatorToken>()->value;

            if (val == Parsing::PunctuatorType::LEFT_PARENTHESIS) {
                parserState = ParserState::IN_FUNCTION_ARGUMENTS;
                return;
            }
        }

        TC_TODO("unrecognized token")
    }

    void TreeConstructor::ProcessState_IN_FUNCTION_ARGUMENTS(const Parsing::ParserToken *token) {
        ++tokenIterator;
        for (; tokenIterator != std::cend(tokens); ++tokenIterator) {
            token = tokenIterator->get();

            if (argumentList.isVoid) {
                if (token->type != Parsing::ParserTokenType::PUNCTUATOR ||
                    token->As<Parsing::PunctuatorToken>()->value != Parsing::PunctuatorType::RIGHT_PARENTHESIS) {
                    InvokeParserError("expected right parenthesis after void argument specifier");
                    return;
                }
            }

            if (token->type == Parsing::ParserTokenType::KEYWORD) {
                auto val = token->As<Parsing::KeywordToken>()->value;

                if (val == Parsing::KeywordTokenType::VOID) {
                    argumentList.isVoid = true;

                    continue;
                }

                std::cout << "TODO> Keyword is " << val << '\n';
                TC_TODO("unrecognized keyword")
            }

            if (token->type == Parsing::ParserTokenType::PUNCTUATOR) {
                auto val = token->As<Parsing::PunctuatorToken>()->value;

                if (val == Parsing::PunctuatorType::RIGHT_PARENTHESIS) {
                    parserState = ParserState::AFTER_FUNCTION_DECLARATION;
                    return;
                }

                std::cout << "TODO> Punctuator is " << val << '\n';
                TC_TODO("unrecognized punctuator")
            }
        }

        TC_TODO("unrecognized token")
    }

    void TreeConstructor::ProcessState_AFTER_FUNCTION_DECLARATION(const Parsing::ParserToken *token) {
        Type type{};

        if (typeToken->type == Parsing::ParserTokenType::KEYWORD) {
            type.isKeyword = true;
            type.keyword = typeToken->As<Parsing::KeywordToken>()->value;
        } else {
            type.isKeyword = false;
            type.structureOrUnionName = typeToken->As<Parsing::IdentifierToken>()->value;
        }

        const auto &declarator = currentBlockNode->functionDeclarations.emplace(
                identifierToken->As<Parsing::IdentifierToken>()->value,
                FunctionDeclarator{
                    std::move(type),
                    std::move(argumentList),
                    nullptr
                }
        ).first;

        declarator->second.name = std::addressof(declarator->first);

        // Cleanup
        typeToken = nullptr;
        identifierToken = nullptr;

        std::cout << "[AST] Got function \"" << declarator->first << "\"\n";

        if (token->type == Parsing::ParserTokenType::PUNCTUATOR) {
            auto val = token->As<Parsing::PunctuatorToken>()->value;
            parserState = ParserState::INITIAL;

            if (val == Parsing::PunctuatorType::SEMICOLON) {
                return;
            }

            if (val == Parsing::PunctuatorType::LEFT_CURLY_BRACKET) {
                auto &node = currentBlockNode->children.emplace_back(
                        std::make_unique<FunctionNode>(currentBlockNode, &declarator->second)
                );

                currentBlockNode = static_cast<BlockNode *>(node.get());
                return;
            }
        }

        TC_TODO("unexpected token")
    }

    void TreeConstructor::ProcessState_AFTER_IDENTIFIER(const Parsing::ParserToken *token) {
        if (token->type == Parsing::ParserTokenType::OPERATOR) {
            auto val = token->As<Parsing::OperatorToken>()->value;

            if (val == Parsing::OperatorType::LEFT_PARENTHESIS) {
                throw std::runtime_error("not supported");
            }

            std::cout << "AfterIdentifier> val=" << val << '\n';
        }

        TC_TODO("unexpected token")
    }

    void TreeConstructor::ProcessState_IN_STATEMENT(const Parsing::ParserToken *token) {
        if (token->type == Parsing::ParserTokenType::PUNCTUATOR) {
            auto val = token->As<Parsing::PunctuatorToken>()->value;

            if (val == Parsing::PunctuatorType::SEMICOLON) {
                // end of statement. nothing to do
                parserState = ParserState::INITIAL;
                return;
            }
        }

        TC_TODO("unexpected token")
    }

    FunctionNode *
    TreeConstructor::GetEnclosingFunction() {
        Node *node = currentBlockNode;

        do {
            if (node->type == NodeType::FUNCTION)
                return static_cast<FunctionNode *>(node);

            node = node->parentNode;
        } while (node);

        return nullptr;
    }

    std::unique_ptr<Expression>
    TreeConstructor::ConsumeExpression() {
        // TODO (5 apr 2021) should be assignment-expression
        return ConsumeAdditiveExpression();
    }

    void TreeConstructor::ConsumeSemiColon() {
        if (tokenIterator != std::end(tokens) &&
                tokenIterator->get()->type == Parsing::ParserTokenType::PUNCTUATOR &&
                tokenIterator->get()->As<Parsing::PunctuatorToken>()->value == Parsing::PunctuatorType::SEMICOLON) {
            ++tokenIterator;
            return;
        }

        InvokeParserError("expected semicolon");
    }

    [[nodiscard]] inline bool
    IsRightParenthesis(const Parsing::ParserToken *token) noexcept {
        if (token->type == Parsing::ParserTokenType::OPERATOR)
            return token->As<Parsing::OperatorToken>()->value == Parsing::OperatorType::RIGHT_PARENTHESIS;

        if (token->type == Parsing::ParserTokenType::PUNCTUATOR)
            return token->As<Parsing::PunctuatorToken>()->value == Parsing::PunctuatorType::RIGHT_PARENTHESIS;

        return false;
    }

    void TreeConstructor::ConsumeFunctionCall() {
//        auto functionCallExpression = std::make_unique<FunctionCallExpression>(identifierToken->As<Parsing::IdentifierToken>()->value);
//        bool requireExpression{false};

        // identifierToken is the name of the function being called
        // the left parenthesis "(" is parsed and the tokenIterator has been
        // increased before this function
//        for (; tokenIterator != std::end(tokens);) {
//            const auto &token = tokenIterator->get();
//
//            std::cout << "[Parsing Arguments of Funccall] " <<  token->type << '\n';
//
//            if (token->type == Parsing::ParserTokenType::PUNCTUATOR)
//                std::cout << "Punctuator> " << token->As<Parsing::PunctuatorToken>()->value << '\n';
//
//            if (IsRightParenthesis(token)) {
//                if (requireExpression) {
//                    InvokeParserError("expected expression");
//                    return;
//                }
//
//                std::cout << "[AST] Function call to \"" << functionCallExpression->functionName << "\" with " << std::size(functionCallExpression->argumentExpressions) << " argument(s)\n";
//
//                currentBlockNode->children.push_back(
//                        std::make_unique<ExpressionStatementNode>(
//                                currentBlockNode,
//                                std::move(functionCallExpression)
//                        )
//                );
//                return;
//            }
//
//            requireExpression = false;
//
//            auto argumentExpression = ConsumeExpression();
//            if (!argumentExpression) {
//                return;
//            }
//
//            functionCallExpression->argumentExpressions.push_back(std::move(argumentExpression));
//
//            std::cout << "Next token is " << tokenIterator->get()->type << '\n';
//            if (tokenIterator != std::end(tokens)) {
//                auto val = tokenIterator->get()->As<Parsing::PunctuatorToken>()->value;
//
//                if (IsRightParenthesis(tokenIterator->get())) {
//                    // handled above
//                    continue;
//                }
//
//                if (tokenIterator->get()->type == Parsing::ParserTokenType::PUNCTUATOR &&
//                        val == Parsing::PunctuatorType::COMMA) {
//                    ++tokenIterator;
//                    requireExpression = true;
//                    continue;
//                }
//            }
//
//            InvokeParserError("unexpected token (expected comma or right parenthesis)");
//            return;
//        }
    }

    std::unique_ptr<Expression>
    TreeConstructor::ConsumePrimaryExpression() {
        const auto &token = **tokenIterator++;

        if (token.type == Parsing::ParserTokenType::CONSTANT_UNSIGNED_INTEGER) {
            std::cout << "ConsumePrimaryExpression> Returning an unsigned integer constant primitive\n";
            return std::make_unique<PrimaryExpression>(token.As<Parsing::UnsignedIntegerConstantToken>()->value);
        }

        if (token.type == Parsing::ParserTokenType::CONSTANT_SIGNED_INTEGER) {
            std::cout << "ConsumePrimaryExpression> Returning a signed integer constant primitive\n";
            return std::make_unique<PrimaryExpression>(token.As<Parsing::SignedIntegerConstantToken>()->value);
        }

        if (token.type == Parsing::ParserTokenType::IDENTIFIER) {
            std::cout << "ConsumePrimaryExpression> Returning an identifier primitive\n";
            return std::make_unique<PrimaryExpression>(&token.As<Parsing::IdentifierToken>()->value);
        }

        InvokeParserError("not a primary expression");
        return nullptr;
    }

    std::unique_ptr<Expression>
    TreeConstructor::ConsumePostfixExpression() {
        auto expression = ConsumePrimaryExpression();
        if (tokenIterator == std::end(tokens)) {
            InvokeParserError("unexpected eof");
        }

        for (; tokenIterator != std::end(tokens); ) {
            const auto type = tokenIterator->get()->type;
            std::cout << "ConsumePostfixExpression> token=" << type << '\n';
            std::cout << "    punctuator=" << tokenIterator->get()->As<Parsing::PunctuatorToken>()->value << '\n';

            if (type == Parsing::ParserTokenType::OPERATOR) {
                const auto operatorType = tokenIterator->get()->As<Parsing::OperatorToken>()->value;
                std::cout << "ConsumePostfixExpression> operator=" << operatorType << '\n';
                if (operatorType == Parsing::OperatorType::LEFT_PARENTHESIS) {
                    const auto &nextToken = (**(tokenIterator + 1));

                    std::cout << "ConsumePostfixExpression> next token=" << nextToken.type << '\n';

                    if (nextToken.type == Parsing::ParserTokenType::OPERATOR &&
                            nextToken.As<Parsing::OperatorToken>()->value == Parsing::OperatorType::RIGHT_PARENTHESIS) {
                        tokenIterator  += 2; // consume parentheses
                        std::cout << "ConsumePostfixExpression> have function call expression\n";
                        expression = std::make_unique<FunctionCallExpression>(std::move(expression));
                        continue;
                    }
                }
            }

            std::cout << "ConsumePostfixExpression> return (can't expand)\n";
            std::cout << "      next token=" << (**tokenIterator).type << '\n';
            return expression;
        }

        std::cout << "ConsumePostfixExpression> return (end of token list)\n";
        return expression;
    }

    std::unique_ptr<Expression>
    TreeConstructor::ConsumeAdditiveExpression() {
        // TODO(4 apr 2021) should be ConsumeMultiplicativeExpression
        auto lhs = ConsumePostfixExpression();

        if (tokenIterator == std::end(tokens)) {
            InvokeParserError("unexpected eof");
        }

        const auto &token = **tokenIterator;
        if (token.type == Parsing::ParserTokenType::OPERATOR) {
            const auto operatorType = token.As<Parsing::OperatorToken>()->value;

            if (operatorType == Parsing::OperatorType::ADDITION ||
                    operatorType == Parsing::OperatorType::SUBTRACTION) {
                ++tokenIterator; // consume operator
                if (tokenIterator == std::end(tokens)) {
                    InvokeParserError("unexpected eof");
                }

                // TODO same as in begin
                auto rhs = ConsumePostfixExpression();
                return std::make_unique<AdditiveExpression>(
                        operatorType == Parsing::OperatorType::ADDITION
                                ? AdditiveExpression::Type::ADDITION
                                : AdditiveExpression::Type::SUBTRACTION,
                        std::move(lhs),
                        std::move(rhs)
                );
            }
        }

        std::cout << "ConsumeAdditiveExpression> Returning from child\n";
        std::cout << "    next token=" << tokenIterator->get()->As<Parsing::PunctuatorToken>()->value << '\n';
        return lhs;
    }

} // namespace AST
