/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <map>
#include <vector>
#include <utility>

#include "Source/Parsing/PreprocessorToken.hpp"
#include "Source/Parsing/RuntimeConstants.hpp"
#include "Source/Text/Unicode.hpp"
#include "Source/Text/UString.hpp"

namespace Parsing {

    struct MacroEntry {
        PreprocessorToken *token;
        std::vector<PreprocessorToken> tokens;

        inline
        MacroEntry(PreprocessorToken *token,
                   std::vector<PreprocessorToken> &&tokens)
                : token(token)
                , tokens(std::move(tokens)) {

        }

        inline
        MacroEntry(PreprocessorToken *token,
                   const std::vector<PreprocessorToken> &tokens)
                : token(token)
                , tokens(tokens) {

        }
    };

    struct PreprocessorData {

        const RuntimeConstants &runtimeConstants;

        bool inNumber{false};

        std::vector<PreprocessorToken> tokens{};

        bool inComment{false};
        bool inCommentAsterisk{false};

        bool inIncludeStatement{false};
        Unicode::CodePoint includeCharacter{Unicode::NULL_CHARACTER};

        //
        // Macro table stuff
        //

        std::map<Unicode::UString, MacroEntry> macroTable{};

        template<typename... Args>
        PreprocessorToken &
        EmplaceToken(Args&&... args) {
            if (!std::empty(tokens)) {
                auto &back = tokens.back();

                if (back.type == PreprocessorTokenType::IDENTIFIER &&
                    back.data == runtimeConstants.operatorSizeof) {
                    back.type = PreprocessorTokenType::OPERATOR;
                }
            }

            return EmplaceTokenInternal(std::forward<Args>(args)...);
        }

    private:
        template<typename... Args>
        PreprocessorToken &
        EmplaceTokenInternal(Args&&... args) {
//            auto &token = tokens.emplace_back(std::forward<Args>(args)...);
//            std::cout << "Emplace token with type=" << token.type << " and data=" << token.data << '\n';
//            return token;
            return tokens.emplace_back(std::forward<Args>(args)...);
        }
    };

} // namespace Parsing
