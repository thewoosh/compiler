# Styling the include directives
Generally speaking, the includes are formatted A-Z, and sorted by the following
sections:
1. C++ standard library includes
2. C standard library includes (prefer C compatibility headers for C++ where
   possible, e.g. <cstdlib> instead of <stdlib.h>)
3. Standard platform headers (e.g. POSIX headers)
4. Platform-specific OS headers (e.g. <linux/>, <sys/>)
5. C++ libraries
6. C libraries
7. Project libraries
8. Any files in the current directory.

The latter may also be included in section 7.

The project libraries section may as well be sorted by module (e.g.
Source/Base/, Source/Text) and by main  section (i.e. Source/, Testing/).

Here follows an example:
```cpp
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <utility>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <strings.h>

#include <unistd.h>

#include <netinet/in.h>
#include <linux/mman.h>
#include <sys/socket.h>

#include <boost/json/detail/config.hpp>

#include <GL/gl.h>
#include <GL/glu.h>
#include <openssl/ssl.h>

#include "Base/ExitCodes.hpp"
#include "Base/Terminate.hpp"

#include "Data/IteratorStream.hpp"
#include "Data/Stream.hpp"
#include "Data/StringStream.hpp"

#include "Text/FastUStrings.hpp"
#include "Text/Unicode.hpp"
#include "Text/UnicodeCommons.hpp"
#include "Text/UnicodeTools.hpp"
#include "Text/UString.hpp"

#include "Generic.hpp"

int main() {
    return Base::ExitCodes::Success;
}
```