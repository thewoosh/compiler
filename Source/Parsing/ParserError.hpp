/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <sstream>
#include <string_view>

#include "Source/Parsing/FormattableMessage.hpp"
#include "Source/Parsing/LinkedPreprocessorToken.hpp"
#include "Source/Parsing/ParserException.hpp"
#include "Source/Parsing/SourceInformation.hpp"

namespace Parsing {

    struct ParserError {
    private:
        ParserException exception;

    public:
        bool revoked{false};
        std::stringstream stream{};

        explicit
        ParserError(std::string_view tag,
                    SourceInformation sourceInformation={}) noexcept
                    : exception{tag, sourceInformation} {
        }

        ParserError(const ParserException &) = delete;

        ~ParserError() noexcept(false) {
            exception.data = stream.str();
            throw exception;
        }

        ParserError &
        operator<<(const auto &v) {
            stream << v;
            return *this;
        }
    };

} // namespace Parsing
