/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <list>
#include <map>
#include <memory>

#include "Source/Base/ClassVisibility.hpp"
#include "Source/Parsing/ParserError.hpp"
#include "Source/Parsing/ParserToken.hpp"
#include "Source/Parsing/PragmaOptions.hpp"
#include "Source/Parsing/PreprocessorData.hpp"
#include "Source/Parsing/RuntimeConstants.hpp"
#include "Source/Parsing/WarningBuilder.hpp"
#include "Source/Text/UString.hpp"

namespace Parsing {

    enum class DefineAction {
        DEFINE,
        UNDEFINE
    };

    class Parser {
    CLASS_VISIBILITY_INTERNAL_MEMBER_STRUCTURES:
        struct Warnings {
            bool macroIdentifierIsKeyword{true};
            bool macroIdentifierIsPredefined{true};
        };

    CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES:
        PragmaOptions pragmaOptions{};
        Warnings warnings{};
        const RuntimeConstants runtimeConstants{};

        Unicode::UString string{};

        Unicode::UString fileName{"main.c"};

        using CharacterIterator = decltype(std::begin(string));
        CharacterIterator characterIt{};

        PreprocessorData preprocessor{runtimeConstants};
        std::map<Unicode::UString, MacroEntry> predefinedMacros{};

        // Used in processing the preprocessor tokens.
        bool validLocationForDirective{true};
        std::vector<std::unique_ptr<ParserToken>> parserTokens{};


    CLASS_VISIBILITY_INTERNAL_MEMBER_FUNCTIONS:
        inline ParserError
        FireParserError(const char *tag) {
            return ParserError(tag, CreateSourceInformation());
        }

        inline WarningBuilder
        FireWarning(const char *tag) {
            return WarningBuilder(
                    tag,
                    CreateSourceInformation(),
            [this](WarningBuilder &warning) {
                        warning.PrintMessage(this, std::cout);
                    }
            );
        }

        [[nodiscard]] SourceInformation
        CreateSourceInformation();

        void CheckDefinableMacroIdentifier(const Unicode::UString &, DefineAction);

        void PreparePredefinedMacros();
        void PrepareSourceInformation();

        void
        SkipWhitespaceInMacro(std::vector<PreprocessorToken>::iterator &,
                              const std::vector<PreprocessorToken>::iterator &);

        void Preprocess();
        void ExecutePreprocessorDirectives();

        void ReplaceEscapeSequences();
        void ConcatenateAdjacentStringLiterals();
        void TranslatePreprocessorTokensToParserTokens();

        void DetectIncludeStatement(const PreprocessorToken &, Unicode::CodePoint);
        void PreprocessComment(Unicode::CodePoint);
        void PreprocessFirst(Unicode::CodePoint);
        void PreprocessFirstNumber(Unicode::CodePoint);
        void PreprocessIncludeStatement(Unicode::CodePoint);
        void PreprocessOperatorOrPunctuator(Unicode::CodePoint);
        void PreprocessDigit(Unicode::CodePoint);

        void AppendSingleOperatorOrPunctuator(Unicode::CodePoint);

        //
        // Translation tools
        //

        [[nodiscard]] OperatorType
        TranslateOperatorType(const PreprocessorToken *);

        [[nodiscard]] OperatorType
        TranslateOperatorTypeAfterEqualsSign(Unicode::CodePoint);

        [[nodiscard]] OperatorType
        TranslateSingleOperatorType(Unicode::CodePoint);

        [[nodiscard]] PunctuatorType
        TranslatePunctuatorType(const PreprocessorToken *);

        void TranslateNumber(const PreprocessorToken *);

        //
        // Macro definition directives
        //

        using PreprocessorTokenIterator = decltype(preprocessor.tokens)::iterator;
        using PreprocessorTokenList = decltype(preprocessor.tokens);

        void ProcessDefineMacro(PreprocessorTokenIterator &,
                                PreprocessorTokenList &);

        void ProcessUndefMacro(decltype(preprocessor.tokens)::iterator &);

        //
        // Macro conditional directives
        //

        void ProcessTokensUntilConditionalDirective(PreprocessorTokenIterator &,
                                                    PreprocessorTokenList &);

        void ConsumeIfStatement(decltype(preprocessor.tokens)::iterator &,
                                decltype(preprocessor.tokens) &);

        void ContinueDirectivesExecutionProcessing(
                PreprocessorTokenIterator &,
                PreprocessorTokenList &
        );

        void ConsumeInvisibleTokensInDirectivesExecution(
                PreprocessorTokenIterator &,
                PreprocessorTokenList &
        );

        void ProcessConditionalMacroExistence(
                PreprocessorTokenIterator &,
                PreprocessorTokenList &,
                bool
        );


        //
        // Pragma directives
        //

        void ProcessPragmaDirective(decltype(preprocessor.tokens)::iterator &,
                                    decltype(preprocessor.tokens) &);

        void ProcessGCCPragmaDirective(std::list<LinkedPreprocessorToken> &&);

        void
        HandleGenericInformationGCCPragma(std::list<LinkedPreprocessorToken> &&pragma,
                                          const Unicode::UString &type, auto &&operand);

        inline void
        EmitParserToken(auto &&token) {
            token->associatedPreprocessorToken = currentToken.source;
            parserTokens.emplace_back(std::forward<decltype(token)>(token));
        }

    public:
        std::vector<Unicode::UString> lines{};
        LinkedPreprocessorToken currentToken{nullptr};

        explicit
        Parser(const Unicode::UString &);

        void Run();
        void RunPreprocessor();

        void PrintDebugInfo();

        [[nodiscard]] inline const auto &
        OutputTokens() const noexcept {
            return parserTokens;
        }
    };

} // namespace Parsing
