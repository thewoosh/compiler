/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/CodeGeneration/FunctionData.hpp"

namespace CodeGeneration {

    struct AssemblyGenerator {
        FunctionData &functionData;

        void AddReturn();
        void AddReturn(std::uint32_t value);

        void SetAccumulator(std::uint32_t);
        void AccumulatorAdd(std::uint32_t);
        void AccumulatorSubtract(std::uint32_t);

        /**
         * @returns A pointer to a 32-bit value to store offset.
         */
        [[nodiscard]] std::uint8_t *
        AddCall();
    };

} // namespace CodeGeneration
