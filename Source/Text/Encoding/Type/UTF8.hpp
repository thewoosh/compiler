/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/Text/Encoding/Encoder.hpp"

namespace Encoding {

    /**
     * Follows the rules of the WHATWG UTF-8 encoding specification.
     * https://encoding.spec.whatwg.org/#utf-8
     */
    template<typename UnitType>
    class UTF8 : public Encoder<UnitType> {
    private:
        Unicode::CodePoint CodePoint;
        std::size_t BytesSeen;
        uint_fast8_t BytesNeeded;
        uint_fast8_t LowerBoundary;
        uint_fast8_t UpperBoundary;

        void ResetUTF8Boundaries() noexcept {
            LowerBoundary = 0x80;
            UpperBoundary = 0xBF;
        }

        void ResetUTF8Parameters() noexcept {
            CodePoint = 0;
            BytesSeen = 0;
            BytesNeeded = 0;
            ResetUTF8Boundaries();
        }


    public:
        UTF8() noexcept {
            ResetUTF8Parameters();
        }

    private:
        /**
         * Decodes an unit of data from the stream.
         *
         * May throw Encoding::EncoderException
         */
        [[nodiscard]] Unicode::CodePoint
        Decode(Data::Stream<UnitType> *) override {
            throw EncoderException(ErrorType::UNIT_NOT_SUPPORTED);
        }
        /**
         * Decodes an unit of data from the stream.
         *
         * May throw Encoding::EncoderException
         */
        void
        Encode(Data::Stream<Unicode::CodePoint> *, std::vector<UnitType> *) override {
            throw EncoderException(ErrorType::UNIT_NOT_SUPPORTED);
        }

        /**
         * See documentation of superclass definition.
         */
        [[nodiscard]] std::size_t
        CalculateDecodeSize(const Data::Stream<UnitType> *) override {
            // TODO
            return 0;
        }

        [[nodiscard]] std::size_t
        CalculateEncodeSize(const Data::Stream<Unicode::CodePoint> *) override {
            // TODO
            return 0;
        }
    };

    template<>
    Unicode::CodePoint
    UTF8<std::uint8_t>::Decode(Data::Stream<std::uint8_t> *);

    template<>
    void
    UTF8<std::uint8_t>::Encode(Data::Stream<Unicode::CodePoint> *, std::vector<std::uint8_t> *);

} // namespace Encoding
