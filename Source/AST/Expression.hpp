/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <vector>

#include <cstdint>

#include "Source/Text/UString.hpp"

namespace AST {

    enum class ExpressionType {
        ADDITIVE,

        PRIMARY_IDENTIFIER,
        PRIMARY_FLOATING_CONSTANT,
        PRIMARY_SIGNED_INTEGER_CONSTANT,
        PRIMARY_UNSIGNED_INTEGER_CONSTANT,
        PRIMARY_ENUMERATION_CONSTANT,
        PRIMARY_CHARACTER_CONSTANT,

        POSTFIX_FUNCTION_CALL,
    };

    struct Expression {
        const ExpressionType type;

        [[nodiscard]] inline constexpr explicit
        Expression(ExpressionType expressionType) noexcept
                : type(expressionType) {
        }

        virtual ~Expression() = default;
    };

    struct PrimaryExpression : public Expression {
        union {
            std::int64_t signedInteger;
            std::uint64_t unsignedInteger;
            const Unicode::UString *identifierName;
        };

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(std::int64_t value) noexcept
                : Expression(ExpressionType::PRIMARY_SIGNED_INTEGER_CONSTANT)
                , signedInteger(value) {
        }

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(std::uint64_t value) noexcept
                : Expression(ExpressionType::PRIMARY_UNSIGNED_INTEGER_CONSTANT)
                , unsignedInteger(value) {
        }

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(const Unicode::UString *identifierName) noexcept
                : Expression(ExpressionType::PRIMARY_IDENTIFIER)
                , identifierName(identifierName) {
        }
    };

    struct FunctionCallExpression : public Expression {
        std::unique_ptr<Expression> operand;
        std::vector<std::unique_ptr<Expression>> argumentExpressions{};

        [[nodiscard]] inline explicit
        FunctionCallExpression(std::unique_ptr<Expression> &&operand) noexcept
                : Expression(ExpressionType::POSTFIX_FUNCTION_CALL)
                , operand(std::move(operand)) {
        }
    };

    struct CombinedExpression : public Expression {
        std::unique_ptr<Expression> left;
        std::unique_ptr<Expression> right;

    protected:
        [[nodiscard]] inline explicit
        CombinedExpression(ExpressionType expressionType, std::unique_ptr<Expression> &&left, std::unique_ptr<Expression> &&right) noexcept
                : Expression(expressionType)
                , left(std::move(left))
                , right(std::move(right)) {
        }
    };

    struct AdditiveExpression : public CombinedExpression {
        enum class Type {
            ADDITION,
            SUBTRACTION
        };

        Type type;

        [[nodiscard]] inline explicit
        AdditiveExpression(Type type, std::unique_ptr<Expression> &&left, std::unique_ptr<Expression> &&right) noexcept
                : CombinedExpression(ExpressionType::ADDITIVE, std::move(left), std::move(right))
                , type(type) {
        }
    };

} // namespace AST
