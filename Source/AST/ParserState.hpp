/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace AST {

#define PARSER_STATE_FOREACH() \
            PARSER_STATE_ELEMENT(INITIAL) \
            PARSER_STATE_ELEMENT(AFTER_TYPE) \
            PARSER_STATE_ELEMENT(AFTER_TYPE_IDENTIFIER) \
            PARSER_STATE_ELEMENT(IN_FUNCTION_ARGUMENTS) \
            PARSER_STATE_ELEMENT(AFTER_FUNCTION_DECLARATION) \
            PARSER_STATE_ELEMENT(AFTER_IDENTIFIER) \
            PARSER_STATE_ELEMENT(IN_STATEMENT) \

    enum class ParserState {
#define PARSER_STATE_ELEMENT(element) element,
        PARSER_STATE_FOREACH()
#undef  PARSER_STATE_ELEMENT
    };


    [[nodiscard]] inline constexpr const char *
    TranslateParserStateToString(ParserState parserState) noexcept {
        switch (parserState) {
#define PARSER_STATE_ELEMENT(element) case ParserState::element: return #element;
        PARSER_STATE_FOREACH()
#undef  PARSER_STATE_ELEMENT
            default: return "(illegal value)";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, ParserState parserState) {
        stream << TranslateParserStateToString(parserState);
        return stream;
    }

} // namespace AST
