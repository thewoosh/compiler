/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Runtime/Engine.hpp"

#include <vector>

#include <cassert>
#include <cstring>

#include <malloc.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Source/CodeGeneration/Generator.hpp"
#include "Source/Data/IteratorStream.hpp"

namespace Runtime {

    Engine::~Engine() noexcept {
        if (buffer) {
            if (mprotect(buffer, bufferSize, PROT_WRITE) == 0) {
                free(buffer);
            }
        }
    }

    bool Engine::AllocateBuffer() {
        if (buffer)
            return true;

        const auto pagesize = sysconf(_SC_PAGE_SIZE);
        if (pagesize == -1) {
            perror("sysconf(_SC_PAGE_SIZE)");
            return false;
        }

        // allocate exactly a page size
        bufferSize = pagesize;

        buffer = static_cast<std::uint8_t *>(memalign(pagesize, bufferSize));
        if (!buffer) {
            perror("allocate page(s) using memalign");
            throw std::runtime_error("Engine::Run failure");
        }

        if (mprotect(buffer, bufferSize, PROT_READ | PROT_WRITE) == -1) {
            perror("mprotect");
            return false;
        }

        return true;
    }

    bool Engine::PlaceFunctions() {
        std::size_t position{0};

        for (const auto &function : generator->Functions()) {
            const auto size = function.second.builder.size();
            assert(size > 0);

            if (position + size > this->bufferSize) {
                return false;
            }

            std::memcpy(buffer + position, function.second.builder.data(), size);

            functionTable.emplace(function.first, buffer + position);

            position += size;
        }

        if (mprotect(buffer, bufferSize, PROT_EXEC) == -1) {
            perror("mprotect");
            return false;
        }

        return true;
    }

    int Engine::Run() {
        if (!AllocateBuffer())
            return -0x8001;
        if (!PlaceFunctions())
            return -0x8002;

        std::vector<const char *> arguments(1);
        std::vector<const char *> environment(1);

        using MainMethodType = int (*)(const char **, int, const char **);

        auto it = functionTable.find(Unicode::UString("main"));
        if (it == std::end(functionTable)) {
            puts("Runtime> Undefined function \"main\"");
            return -0x8003;
        }

        auto func = reinterpret_cast<MainMethodType>(const_cast<void *>(it->second));
        return func(std::data(arguments), std::size(arguments), std::data(environment));
    }

} // namespace Runtime
