/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/AST/ArgumentList.hpp"
#include "Source/AST/Type.hpp"

namespace AST {

    struct FunctionDeclarator {
        Type returnType;
        ArgumentList argumentList;
        const Unicode::UString *name;

        [[nodiscard]] inline
        FunctionDeclarator(Type &&returnType, ArgumentList &&argumentList, const Unicode::UString *name)
                : returnType(std::move(returnType))
                , argumentList(std::move(argumentList))
                , name(name) {
        }
    };

} // namespace AST
