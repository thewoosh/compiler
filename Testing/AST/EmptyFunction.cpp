/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <string_view>
#include <vector>

#define COMPILER_TESTING
#include "gtest/gtest.h"

#include "Source/AST/TreeConstructor.hpp"
#include "Source/Parsing/Parser.hpp"
#include "Source/Parsing/PreprocessorToken.hpp"

TEST(EmptyFunctionTests, SimpleTest) {
    Unicode::UString string("int main(void) {}");

    Parsing::Parser parser(string);
    parser.Run();

    AST::TreeConstructor treeConstructor{parser.OutputTokens()};
    treeConstructor.Run();

    ASSERT_EQ(treeConstructor.rootNode.children.size(), 1);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->type, AST::NodeType::FUNCTION);
    ASSERT_TRUE(treeConstructor.rootNode.children[0]->children.empty());

    const auto *function = static_cast<const AST::FunctionNode *>(treeConstructor.rootNode.children[0].get());
    EXPECT_TRUE(function->declarator->returnType.isKeyword);
    EXPECT_EQ(function->declarator->returnType.keyword, Parsing::KeywordTokenType::INT);

    ASSERT_EQ(treeConstructor.rootNode.functionDeclarations.size(), 1);
    EXPECT_EQ(treeConstructor.rootNode.functionDeclarations.begin()->first, Unicode::UString("main"));
    EXPECT_EQ(&treeConstructor.rootNode.functionDeclarations.begin()->second, function->declarator);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
