/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <string_view>
#include <vector>

#define COMPILER_TESTING
#include "gtest/gtest.h"

#include "Source/Parsing/Parser.hpp"
#include "Source/Parsing/PreprocessorToken.hpp"

Unicode::UString sourceFileName{"test.c"};

Parsing::SourceInformation sourceInformation{
    sourceFileName, 0, 0
};

void ExecuteTest(std::string_view sv,
                 const std::vector<Parsing::PreprocessorToken> &expectedTokens) {
    Unicode::UString data{sv};
    Parsing::Parser parser(data);

    parser.RunPreprocessor();

    ASSERT_LE(std::size(expectedTokens), std::size(parser.preprocessor.tokens));

    std::size_t j = 0;
    for (std::size_t i = 0; i < std::size(parser.preprocessor.tokens); ++i) {
        if (parser.preprocessor.tokens[i].type == Parsing::PreprocessorTokenType::WHITESPACE)
            continue;

        ASSERT_EQ(parser.preprocessor.tokens[i].type, expectedTokens[j].type);
        ASSERT_EQ(parser.preprocessor.tokens[i].data, expectedTokens[j].data);

        j++;
    }
}

[[nodiscard]] inline Parsing::PreprocessorToken
CreateIdentifierToken(std::string_view sv) {
    return Parsing::PreprocessorToken{Parsing::SourceInformation{sourceInformation}, Parsing::PreprocessorTokenType::IDENTIFIER, Unicode::UString{sv}};
}

TEST(ConditionalDirectives, SimpleIfdefTest) {
    ExecuteTest(R"(
    #ifdef __FUNCTION__
        some_identifier_name
    #endif
    )", {
        CreateIdentifierToken("some_identifier_name")
    });
}

TEST(ConditionalDirectives, SimpleIfndefTest) {
    ExecuteTest(R"(
    #ifndef __FUNCTION__
        some_identifier_name
    #endif
    )", {});
}

TEST(ConditionalDirectives, SimpleIfdefElseTest) {
    ExecuteTest(R"(
    #ifdef NON_EXISTING_MACRO
        identifier_when_macro_exists
    #else
        identifier_when_macro_doesnt_exist
    #endif
    )", {
        CreateIdentifierToken("identifier_when_macro_doesnt_exist")
    });
}

TEST(ConditionalDirectives, SimpleIfndefElseTest) {
    ExecuteTest(R"(
    #ifndef NON_EXISTING_MACRO
        identifier_when_macro_exists
    #else
        identifier_when_macro_doesnt_exist
    #endif
    )", {
            CreateIdentifierToken("identifier_when_macro_exists")
    });
}

TEST(ConditionalDirectives, NestedIfdefTest) {
    ExecuteTest(R"(
    #ifdef __FUNCTION__
        #ifdef __LINE__
            builtins_function_and_line_exist
        #endif
    #endif
    )", {
            CreateIdentifierToken("builtins_function_and_line_exist")
    });
}

TEST(ConditionalDirectives, NestedIfndefTest) {
    ExecuteTest(R"(
    #ifndef __cplusplus
        #ifndef __STDC_VERSION__
            ansi_c_detected
        #endif
    #endif
    )", {
            CreateIdentifierToken("ansi_c_detected")
    });
}

TEST(ConditionalDirectives, NestedCombinedDefinedTest) {
    ExecuteTest(
            "#ifndef __cplusplus\n"
            "  #ifndef __STDC_VERSION__\n"
            "    #ifdef __STDC__\n"
            "      ansi_c_detected\n"
            "    #else\n"
            "      knr_c_detected\n"
            "    #endif\n"
            "  #else\n"
            "    c_99_or_above_detected\n"
            "  #endif\n"
            "#else\n"
            "  cpp_detected\n"
            "#endif\n", {
            CreateIdentifierToken("ansi_c_detected")
    });
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
