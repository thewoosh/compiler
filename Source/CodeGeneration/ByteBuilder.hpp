/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include <cstring>

#include "Source/Base/ClassVisibility.hpp"
#include "Source/Base/Portability.hpp"

namespace CodeGeneration {

    class ByteBuilder {
    CLASS_VISIBILITY_INTERNAL_MEMBER_VARIABLES:
        std::vector<std::uint8_t> internalData{};

    public:
        [[nodiscard]] inline BASE_PORTABILITY_CXX20_CONSTEXPR std::size_t
        size() const noexcept {
            return internalData.size();
        }

        [[nodiscard]] inline BASE_PORTABILITY_CXX20_CONSTEXPR std::uint8_t *
        data() noexcept {
            return internalData.data();
        }

        [[nodiscard]] inline BASE_PORTABILITY_CXX20_CONSTEXPR const std::uint8_t *
        data() const noexcept {
            return internalData.data();
        }

        [[nodiscard]] inline BASE_PORTABILITY_CXX20_CONSTEXPR std::uint8_t &
        operator[](std::size_t index) noexcept {
            return internalData[index];
        }

        [[nodiscard]] inline BASE_PORTABILITY_CXX20_CONSTEXPR std::uint8_t
        operator[](std::size_t index) const noexcept {
            return internalData[index];
        }

        inline void AppendOctet(std::uint8_t value) {
            internalData.push_back(value);
        }

        inline void AppendWord(std::uint16_t value) {
            internalData.resize(std::size(internalData) + 2);
            std::memcpy(&*internalData.end() - 2, &value, 2);
        }

        inline void AppendDword(std::uint32_t value) {
            internalData.resize(std::size(internalData) + 4);
            std::memcpy(&*internalData.end() - 4, &value, 4);
        }
    };

} // namespace CodeGeneration
