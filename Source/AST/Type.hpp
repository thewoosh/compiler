/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Parsing/KeywordTokenType.hpp"
#include "Source/Text/UString.hpp"

namespace AST {

    struct Type {
        bool isKeyword{};

        Parsing::KeywordTokenType keyword{};
        Unicode::UString structureOrUnionName{};
    };

} // namespace AST
