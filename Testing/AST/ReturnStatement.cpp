/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <string_view>
#include <vector>

#define COMPILER_TESTING
#include "gtest/gtest.h"

#include "Source/AST/TreeConstructor.hpp"
#include "Source/Parsing/Parser.hpp"
#include "Source/Parsing/PreprocessorToken.hpp"

TEST(ReturnStatementTests, SimpleTest) {
    Unicode::UString string(
            R"(int main(void) {
                return 123;
            })"
    );

    Parsing::Parser parser(string);
    parser.Run();

    AST::TreeConstructor treeConstructor{parser.OutputTokens()};
    treeConstructor.Run();

    ASSERT_EQ(treeConstructor.rootNode.children.size(), 1);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->type, AST::NodeType::FUNCTION);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->children.size(), 1);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->children[0]->type, AST::NodeType::STATEMENT_RETURN);

    const auto *returnStatement = static_cast<const AST::ReturnStatementNode *>(treeConstructor.rootNode.children[0]->children[0].get());
    ASSERT_EQ(returnStatement->expression->type, AST::ExpressionType::PRIMARY_UNSIGNED_INTEGER_CONSTANT);

    const auto *integerConstant = static_cast<const AST::PrimaryExpression *>(returnStatement->expression.get());
    ASSERT_EQ(integerConstant->unsignedInteger, 123);
}

TEST(ReturnStatementTests, ComplexerExpression) {
    Unicode::UString string(
            R"(int main(void) {
                return 52 + 86;
            })"
    );

    Parsing::Parser parser(string);
    parser.Run();

    AST::TreeConstructor treeConstructor{parser.OutputTokens()};
    treeConstructor.Run();

    ASSERT_EQ(treeConstructor.rootNode.children.size(), 1);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->type, AST::NodeType::FUNCTION);
    ASSERT_EQ(treeConstructor.rootNode.children[0]->children.size(), 1);

    ASSERT_EQ(treeConstructor.rootNode.children[0]->children[0]->type, AST::NodeType::STATEMENT_RETURN);
    const auto *returnStatement = static_cast<const AST::ReturnStatementNode *>(treeConstructor.rootNode.children[0]->children[0].get());

    ASSERT_EQ(returnStatement->expression->type, AST::ExpressionType::ADDITIVE);
    const auto *expression = static_cast<const AST::AdditiveExpression *>(returnStatement->expression.get());

    EXPECT_EQ(expression->type, AST::AdditiveExpression::Type::ADDITION);

    ASSERT_NE(expression->left, nullptr);
    ASSERT_NE(expression->right, nullptr);
    ASSERT_EQ(expression->left->type, AST::ExpressionType::PRIMARY_UNSIGNED_INTEGER_CONSTANT);
    ASSERT_EQ(expression->right->type, AST::ExpressionType::PRIMARY_UNSIGNED_INTEGER_CONSTANT);

    ASSERT_EQ(static_cast<const AST::PrimaryExpression *>(expression->left.get())->unsignedInteger, 52);
    ASSERT_EQ(static_cast<const AST::PrimaryExpression *>(expression->right.get())->unsignedInteger, 86);
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
